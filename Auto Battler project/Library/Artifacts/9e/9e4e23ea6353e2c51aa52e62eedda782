                           6Đ               0.0.0 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙   Ŕ           1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               \     ˙˙˙˙               H r   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                     Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                        \       ŕyŻ     `       p&                                                                                                                                                ŕyŻ                                                                                    ChessBoardTile  ö%  using UnityEngine;

namespace AutoBattles
{
    /// <summary>
    /// Class with information about a chessboard tile
    /// </summary>
    public class ChessBoardTile : MonoBehaviour
    {
        #region Variables
        [Header("Pawn Info")]
        [SerializeField]
        private GameObject _activePawn;

        [Header("Tile Info")]
        [SerializeField]
        private int _id;
        [SerializeField]
        private Vector2 _gridPosition;
        [SerializeField]
        private TileCategory _tileType;
        [SerializeField]
        private float _defaultPawnYRotation;

        [Header("Visual References")]
        [SerializeField]
        private GameObject _hoverHighlight;

        //References
        private PawnDragManager _pawnDragScript;
        private GameManager _gameManager;
        private ArmyManager _armyManagerScript;
        private UserInterfaceManager _uiManager;

        public enum TileCategory
        {
            None,
            Player,
            Enemy,
            Bench
        }
        #endregion

        #region Properties
        //this holds the info of the active pawn this tile currently owns
        public GameObject ActivePawn { get => _activePawn; protected set => _activePawn = value; }

        //this is set when the tile is created
        public int Id { get => _id; protected set => _id = value; }

        //this is the tiles position on the total chess board grid
        //used for movement calculating
        public Vector2 GridPosition { get => _gridPosition; protected set => _gridPosition = value; }

        //this is the type of tile
        public TileCategory TileType { get => _tileType; protected set => _tileType = value; }

        //This will be set at the time of Setup based on the TileCategory set at that time
        protected float DefaultPawnYRotation { get => _defaultPawnYRotation; set => _defaultPawnYRotation = value; }

        protected GameObject HoverHighlight { get => _hoverHighlight; set => _hoverHighlight = value; }

        //references
        protected PawnDragManager PawnDragScript { get => _pawnDragScript; set => _pawnDragScript = value; }
        protected GameManager GameManager { get => _gameManager; set => _gameManager = value; }
        protected ArmyManager ArmyManagerScript { get => _armyManagerScript; set => _armyManagerScript = value; }
        protected UserInterfaceManager UIManager { get => _uiManager; set => _uiManager = value; }

        #endregion

        #region Methods

        //Initialization
        protected virtual void Awake()
        {
            if (!HoverHighlight)
            {
                Debug.LogError("No hoverHighlight reference set on the ChessBoardTile script on Chess Board Tile prefab. Please drag 'Highlight' child on" +
                    "Chess Board Tile prefab into the ChessBoardTile script in the inspector.");
            }

            PawnDragScript = PawnDragManager.Instance;
            if (!PawnDragScript)
            {
                Debug.LogError("No PawnDragManager singleton instance found in the scene. Please add a PawnDragManager script to the Game Manager gameobject before entering playmode!");
            }

            ArmyManagerScript = ArmyManager.Instance;
            if (!ArmyManagerScript)
            {
                Debug.LogError("No ArmyManager singleton instance found in the scene. Please add a ArmyManager script to the Game Manager gameobject before entering playmode!");
            }

            GameManager = GameManager.Instance;
            if (!GameManager)
            {
                Debug.LogError("No GameManager singleton instance found in the scene. Please add an AutoBattles.GameManager script to the game manager gameobject " +
                    "before entering playmode!");
            }

            UIManager = UserInterfaceManager.Instance;
            if (!UIManager)
            {
                Debug.LogError("No UserInterfaceManager singleton instance found in the scene. Please add an AutoBattles.GameManager script to the game manager gameobject " +
                    "before entering playmode!");
            }
        }

        #region Used for setting up the tile at runtime
        public virtual void Setup(int id, Vector2 gridPosition, TileCategory tileType)
        {
            Id = id;

            GridPosition = gridPosition;

            TileType = tileType;

            SetDefaultYRotation(TileType);
        }

        private void SetDefaultYRotation(TileCategory tileCategory)
        {
            if (tileCategory == TileCategory.Bench)
            {
                DefaultPawnYRotation = 180f;
            }
            else if (tileCategory == TileCategory.Enemy)
            {
                DefaultPawnYRotation = 180f;
            }
            else if (tileCategory == TileCategory.Player)
            {
                DefaultPawnYRotation = 0f;
            }
        }
        #endregion

        //Returns true if we currenly have an active pawn.
        public virtual bool HasActivePawn()
        {
            if (ActivePawn != null)
                return true;
            else
                return false;
        }

        public virtual void ClearActivePawn()
        {
            ActivePawn = null;
        }

        //Creates a new player pawn, called when pawn is upgraded or when player buys a new pawn from the shop.
        public virtual void CreatePlayerPawn(PawnStats pawnStats, int goldCost)
        {
            //delete old pawn (this will only happen when a pawn is upgrading)
            if (ActivePawn != null)
            {
                Destroy(ActivePawn);
            }

            //Instantiate the actual pawn gameobject
            ActivePawn = Instantiate(pawnStats.pawn);

            Status status = ActivePawn.GetComponent<Status>();
            status.IsPlayer = true;
            status.GoldWorth = goldCost;

            //add this newly created pawn to the players total roster
            ArmyManagerScript.AddPawnToTotalPlayerRoster(ActivePawn);

            //still call ChangePawn() because we want to be sure to set
            //this newly created pawn up the same way as all other existing
            //pawns would be when entering this tile
            ChangePawnOutOfCombat(ActivePawn);
        }


        //This function will be called when swapping pawns around the chessboard or bench out of combat.
        public virtual void ChangePawnOutOfCombat(GameObject newPawn)
        {
            //if we are changing this slot to no pawn
            //clear the active pawn and exit the function
            if (newPawn == null)
            {
                ClearActivePawn();
                return;
            }

            //this will set the pawns home base for later use when
            //dragging or after a round of combat has ended
            newPawn.GetComponent<HomeBase>().SetHomeBase(this);

            //let the pawn know which tile it is currently residing on
            //this is seperate from the homebase
            newPawn.GetComponent<Movement>().SetCurrentTileOutOfCombat(this);

            //change our active pawn
            ActivePawn = newPawn;

            //update new pawns position
            ActivePawn.transform.position = transform.position;

            //update new pawns rotation
            ActivePawn.transform.rotation = Quaternion.Euler(0, DefaultPawnYRotation, 0);
        }

        public virtual void ChangePawnInCombat(GameObject newPawn)
        {
            //if we are changing this slot to no pawn
            //clear the active pawn and exit the function
            if (newPawn == null)
            {
                ClearActivePawn();
                return;
            }

            //let the pawn know which tile it is currently residing on
            //this is seperate from the homebase, if the pawn had an old tile
            //this function call will also let that tile know the pawn moved
            newPawn.GetComponent<Movement>().SetCurrentTileInCombat(this);

            //change our active pawn
            ActivePawn = newPawn;
        }
        #endregion

        #region Mouse Functions

        protected virtual void OnMouseEnter()
        {
            //on mouse enter this specific tile, enable the tile highlight
            HoverHighlight.SetActive(true);

            //tell the PawnDragManager script that we are hovered over this specific tile
            PawnDragScript.SetHoveredTile(this);

            ItemDragAndDrop item = UIManager.FindDraggedItem();

            if (item)
            {
                item.HoveredPawn = ActivePawn;
            }
        }

        protected virtual void OnMouseExit()
        {
            //when mouse exits this specific tile, turn off the tile hightlight
            HoverHighlight.SetActive(false);

            //tell the PawnDragManager script that we are no longer hovered over this tile
            PawnDragScript.SetHoveredTile(null);

            ItemDragAndDrop item = UIManager.FindDraggedItem();

            if (item)
            {
                item.HoveredPawn = null;
            }
        }

        protected virtual void OnMouseDown()
        {
            //let the pawn drag manager script know we just clicked on
            //this tile/pawn
            PawnDragScript.ClickPawn(ActivePawn, this);
        }       

        #endregion
    }
}

                         ChessBoardTile     AutoBattles 