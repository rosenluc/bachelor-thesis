﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// In this script we will define and assign our buffs to their appropriate synergies
/// 
/// Each Synergy should have its own section containing:
/// 
/// -A reference to the synergy
/// -Initialization of the synergy which includes assigning the buff functions 
///     to the appropriate delegates inside the synergy
/// -The actual buff functions themselves that take in a List<GameObject> of pawns to apply the
///     buffs/debuffs to
/// </summary>

namespace AutoBattles
{
    public class SynergyBuffsAndDebuffs : Singleton<SynergyBuffsAndDebuffs>
    {
        #region Variables       

        #endregion

        #region Properties

        #endregion

        #region Methods
        protected virtual void Awake()
        {
            //Initialize our synergies
            InitializeWarriorSynergy();
            InitializeOrcSynergy();
            InitializeKnightSynergy();
            InitializeHumanSynergy();
        }

        //we will assign this function to any remaining buff delegates in our synergies that will
        //not be used. This is to avoid compiler errors in the event we accidently call a buff
        //that is null, it will instead trigger this empty function and do nothing and let us know
        protected virtual void Empty(List<GameObject> pawns)
        {
            Debug.LogWarning("Called 'Empty' function in SynergyBuffsAndDebuffs script. Make sure your synergies are properly" +
                "initialized and setup.");
        }

        #region WARRIOR SYNERGY

        [Header("WARRIOR SYNERGY")]
        [SerializeField]
        private Synergy _warriorSynergy;

        protected Synergy WarriorSynergy { get => _warriorSynergy; set => _warriorSynergy = value; }

        protected virtual void InitializeWarriorSynergy()
        {
            if (!WarriorSynergy)
            {
                Debug.LogError("No 'WarriorSynergy' reference set on the SynergyBuffsAndDebuffs script on the GameManager gameobject. Please set a reference to this synergy before entering playmode!");
                return;
            }

            //set our delegates up
            WarriorSynergy.buff1 = WARRIOR_BUFF_1;
            WarriorSynergy.buff2 = WARRIOR_BUFF_2;
            WarriorSynergy.buff3 = Empty;            
        }

        // This section contains all the warrior synergy buffs

        public virtual void WARRIOR_BUFF_1(List<GameObject> pawns)
        {
            //the amount of physical, magical and true damage we want to increase for each affected pawn
            float physicalDmg = 10;
            float magicDmg = 10;
            float trueDmg = 10;

            foreach (GameObject pawn in pawns)
            {
                //add the damage to both auto and special attack
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                pawnScript.AAPhysicalDamage += physicalDmg;
                pawnScript.AAMagicDamage += magicDmg;
                pawnScript.AATrueDamage += trueDmg;

                pawnScript.SAPhysicalDamage += physicalDmg;
                pawnScript.SAMagicDamage += magicDmg;
                pawnScript.SATrueDamage += trueDmg;

                //note how much we added so we can later remove it
                pawnScript.SynergyPhysicalDamage = physicalDmg;
                pawnScript.SynergyMagicDamage = magicDmg;
                pawnScript.SynergyTrueDamage = trueDmg;
            }
        }        

        public virtual void WARRIOR_BUFF_2(List<GameObject> pawns)
        {
            //the amount of physical, magical and true damage we want to increase for each affected pawn
            float physicalDmg = 20;
            float magicDmg = 20;
            float trueDmg = 20;

            foreach (GameObject pawn in pawns)
            {
                //add the damage to both auto and special attack
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                pawnScript.AAPhysicalDamage += physicalDmg;
                pawnScript.AAMagicDamage += magicDmg;
                pawnScript.AATrueDamage += trueDmg;

                pawnScript.SAPhysicalDamage += physicalDmg;
                pawnScript.SAMagicDamage += magicDmg;
                pawnScript.SATrueDamage += trueDmg;

                //note how much we added so we can later remove it
                pawnScript.SynergyPhysicalDamage = physicalDmg;
                pawnScript.SynergyMagicDamage = magicDmg;
                pawnScript.SynergyTrueDamage = trueDmg;
            }
        }

        #endregion

        #region ORC SYNERGY
        [Header("ORC SYNERGY")]
        [SerializeField]
        private Synergy _orcSynergy;

        protected Synergy OrcSynergy { get => _orcSynergy; set => _orcSynergy = value; }

        protected virtual void InitializeOrcSynergy()
        {
            if (!OrcSynergy)
            {
                Debug.LogError("No 'OrcSynergy' reference set on the SynergyBuffsAndDebuffs script on the GameManager gameobject. Please set a reference to this synergy before entering playmode!");
                return;
            }

            OrcSynergy.buff1 = ORC_BUFF_1;
            OrcSynergy.buff2 = ORC_BUFF_2;
            OrcSynergy.buff3 = Empty;
        }

        // This section contains all the orc synergy buffs

        protected virtual void ORC_BUFF_1(List<GameObject> pawns)
        {
            //the amount of healing, armor and resistance we want to increase for each affected pawn
            float healingBuff = 10;
            float armorBuff = 3;
            float resistanceBuff = 3;

            foreach (GameObject pawn in pawns)
            {
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                //add the healing, armor and resistance
                pawnScript.Healing += healingBuff;
                //note how much we added so we can later remove it
                pawnScript.SynergyHealing = healingBuff;

                pawnScript.Armor += armorBuff;
                pawnScript.SynergyArmor = armorBuff;

                pawnScript.Resistance += resistanceBuff;
                pawnScript.SynergyResistance = resistanceBuff;
            }
        }

        protected virtual void ORC_BUFF_2(List<GameObject> pawns)
        {
            //the amount of healing, armor and resistance we want to increase for each affected pawn
            float healingBuff = 15;
            float armorBuff = 8;
            float resistanceBuff = 8;

            foreach (GameObject pawn in pawns)
            {
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                //add the healing, armor and resistance
                pawnScript.Healing += healingBuff;
                //note how much we added so we can later remove it
                pawnScript.SynergyHealing = healingBuff;

                pawnScript.Armor += armorBuff;
                pawnScript.SynergyArmor = armorBuff;

                pawnScript.Resistance += resistanceBuff;
                pawnScript.SynergyResistance = resistanceBuff;
            }
        }


        #endregion

        #region KNIGHT SYNERGY
        [Header("KNIGHT SYNERGY")]
        [SerializeField]
        private Synergy _knightSynergy;

        protected Synergy KnightSynergy { get => _knightSynergy; set => _knightSynergy = value; }

        protected virtual void InitializeKnightSynergy()
        {
            if (!KnightSynergy)
            {
                Debug.LogError("No 'KnightSynergy' reference set on the SynergyBuffsAndDebuffs script on the GameManager gameobject. Please set a reference to this synergy before entering playmode!");
                return;
            }

            KnightSynergy.buff1 = KNIGHT_BUFF_1;
            KnightSynergy.buff2 = KNIGHT_BUFF_2;
            KnightSynergy.buff3 = Empty;
        }

        // This section contains all the knight synergy buffs

        protected virtual void KNIGHT_BUFF_1(List<GameObject> pawns)
        {
            //the number we want to multiply buffs and debuffs placed on knights
            //this means buffs will be 5% stronger and debuffs will be unaffected
            float buff = 1.05f;
            float debuff = 1;

            foreach (GameObject pawn in pawns)
            {
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                //add the values to the affected pawns
                pawnScript.SynergyBuffs = buff;
                pawnScript.SynergyDebuffs = debuff;
            }
        }

        protected virtual void KNIGHT_BUFF_2(List<GameObject> pawns)
        {
            //the number we want to multiply buffs and debuffs placed on knights
            //this means buffs will be 7% stronger and debuffs will be 3% weaker
            float buff = 1.07f;
            float debuff = 0.97f;

            foreach (GameObject pawn in pawns)
            {
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                //add the values to the affected pawns
                pawnScript.SynergyBuffs = buff;
                pawnScript.SynergyDebuffs = debuff;
            }
        }

        #endregion

        #region HUMAN SYNERGY
        [Header("HUMAN SYNERGY")]
        [SerializeField]
        private Synergy _humanSynergy;

        protected Synergy HumanSynergy { get => _humanSynergy; set => _humanSynergy = value; }

        protected virtual void InitializeHumanSynergy()
        {
            if (!HumanSynergy)
            {
                Debug.LogError("No 'HumanSynergy' reference set on the SynergyBuffsAndDebuffs script on the GameManager gameobject. Please set a reference to this synergy before entering playmode!");
                return;
            }

            HumanSynergy.buff1 = HUMAN_BUFF_1;
            HumanSynergy.buff2 = HUMAN_BUFF_2;
            HumanSynergy.buff3 = HUMAN_BUFF_3;
        }

        // This section contains all the human synergy buffs

        protected virtual void HUMAN_BUFF_1(List<GameObject> pawns)
        {
            //the amount of gold we get for a surviving human pawn
            float gold = 0.5f;

            foreach (GameObject pawn in pawns)
            {
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                //add the value to the affected pawn
                pawnScript.SynergyGold = gold;
            }
        }

        protected virtual void HUMAN_BUFF_2(List<GameObject> pawns)
        {
            //the amount of gold we get for a surviving human pawn
            float gold = 0.5f;

            foreach (GameObject pawn in pawns)
            {
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                //add the value to the affected pawn
                pawnScript.SynergyGold = gold;
            }
        }

        protected virtual void HUMAN_BUFF_3(List<GameObject> pawns)
        {
            //the amount of gold we get for a surviving human pawn
            float gold = 1;

            foreach (GameObject pawn in pawns)
            {
                Pawn pawnScript = pawn.GetComponent<Pawn>();
                //add the value to the affected pawn
                pawnScript.SynergyGold = gold;
            }
        }

        #endregion

        #endregion
    }
}

