using AutoBattles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Special attack is performed once in a while, when pawn fully stacks their mana bar
/// </summary>
public class SpecialAttack : MonoBehaviour
{
    #region Variables
    private bool _active;
    //References
    private Targeting _targetingScript;
    private Pawn _pawnScript;
    private Movement _movementScript;
    private Animator _anim;
    private Status _statusScript;
    private HealthAndMana _healthAndManaScript;
    #endregion

    #region Properties
    //true if special attack is currently active
    //this is used so that pawns cannot gain mana while performing the special attack
    public bool Active { get => _active; set => _active = value; }
    //references
    protected Targeting TargetingScript { get => _targetingScript; set => _targetingScript = value; }
    protected Pawn PawnScript { get => _pawnScript; set => _pawnScript = value; }
    protected Movement MovementScript { get => _movementScript; set => _movementScript = value; }
    protected Animator Anim { get => _anim; set => _anim = value; }
    protected Status StatusScript { get => _statusScript; set => _statusScript = value; }
    protected HealthAndMana HealthAndManaScript { get => _healthAndManaScript; set => _healthAndManaScript = value; }
    #endregion

    #region Methods
    void Awake()
    {
        TargetingScript = GetComponent<Targeting>();
        if (!TargetingScript)
        {
            Debug.LogError("SpecialAttack script object has no Targeting component.");
        }
        PawnScript = GetComponent<Pawn>();
        if (!PawnScript)
        {
            Debug.LogError("SpecialAttack script object has no Pawn component.");
        }
        MovementScript = GetComponent<Movement>();
        if (!MovementScript)
        {
            Debug.LogError("SpecialAttack script object has no Movement component.");
        }
        StatusScript = GetComponent<Status>();
        if (!StatusScript)
        {
            Debug.LogError("SpecialAttack script object has no Status component.");
        }
        HealthAndManaScript = GetComponent<HealthAndMana>();
        if (!HealthAndManaScript)
        {
            Debug.LogError("SpecialAttack script object has no HealthAndMana component.");
        }
        Active = false;
    }

    //finds out which attack is supposed to be performed
    //from pawn's name
    public void LaunchAttack()
    {
        switch (PawnScript.name.Substring(0, 2))
        {
            case "As":
                AssassinSA();
                break;
            case "Co":
                CommanderSA();
                break;
            case "Cr":
                CrossbowSA();
                break;
            case "Ki":
                KingSA();
                break;
            case "Li":
                LindaSA();
                break;
            case "Ma":
                MaceSA();
                break;
            case "Sw":
                SwordSA();
                break;
            case "Te":
                TeenySA();
                break;
        }
    }

    //assassin's special attack
    //deals damage to enemy with most damage
    private void AssassinSA()
    {
        //find target with most damage this round
        HealthAndMana targetHealth = TargetingScript.SearchForTargetWithMostDamage();

        if (targetHealth)
        {
            //deal damage to the target
            targetHealth.TakeDamage(PawnScript, PawnScript.SAPhysicalDamage, PawnScript.AAMagicDamage, PawnScript.AATrueDamage);
            //add damage to stat tracking
            PawnScript.GetComponent<IngameStats>().AddDamageDealt(PawnScript.SAPhysicalDamage, PawnScript.SAMagicDamage, PawnScript.SATrueDamage);

            //perform item funcitons (if they don't have the item, value will be 0)
            float mana = PawnScript.SAMagicDamage * PawnScript.ItemMagicDmgToMana;
            HealthAndManaScript.GainMana(mana);

            float heal = PawnScript.SATrueDamage * PawnScript.ItemTrueDamageToHealth;
            HealthAndManaScript.GainHealth(heal);
            //add health to stat tracking
            PawnScript.GetComponent<IngameStats>().AddHealthHealed(0, heal);

            //if pawn has item, add values to the item stat tracking
            if (PawnScript.HasItem)
            {
                PawnScript.EquippedItem.StatHealth += heal;
                PawnScript.EquippedItem.StatMana += mana;
            }

            //paint target red for 0.3s
            targetHealth.Paint(1, 0, 0, 0.3f);
        }
    }

    //commander's special attack
    //lowers mana regeneration and mana per attack of all targets in range
    private void CommanderSA()
    {
        //note start of the attack
        Active = true;
        //find all targets in range
        List<GameObject> targets = TargetingScript.SearchForNearbyTargets(PawnScript.SARange * 5.1f);
        foreach (GameObject t in targets)
        {
            Pawn p = t.GetComponent<Pawn>();
            //calculate, how much the attack will actually affect the target considering items, synergies etc.
            float b1 = PawnScript.SAManaRegen * 0.01f * (1 - p.SynergyDebuffs) * (1 - PawnScript.ItemBuff);
            p.ManaRegen *= b1;
            float b2 = PawnScript.SAManaPerAttack * 0.01f * (1 - p.SynergyDebuffs) * (1 - PawnScript.ItemBuff);
            p.ManaPerAttack *= b2;
            //paint mana bar of the affected target to violet for duration of the attack
            t.GetComponent<HealthAndMana>().PaintManaBar(0.82f, 0.05f, 0.69f, PawnScript.SADuration);

            //if pawn has item, add values to the item stat tracking
            if (PawnScript.HasItem)
            {
                PawnScript.EquippedItem.StatBuff += b1 - PawnScript.SAManaRegen * 0.01f * (1 - p.SynergyDebuffs);
                PawnScript.EquippedItem.StatBuff += b2 - PawnScript.SAManaPerAttack * 0.01f * (1 - p.SynergyDebuffs);
            }
        }
        //wait and then remove debuffs
        StartCoroutine(CommTimer(targets));
    }

    //removes effects commander's special attack
    private IEnumerator CommTimer(List<GameObject> targets)
    {
        //wait for the time the attack is supposed to last
        yield return new WaitForSeconds(PawnScript.SADuration);

        //remove effect from all affected targets
        foreach (GameObject t in targets)
        {
            Pawn p = t.GetComponent<Pawn>();
            p.ManaRegen /= PawnScript.SAManaRegen * 0.01f * (1 - p.SynergyDebuffs) * (1 - PawnScript.ItemBuff);
            p.ManaPerAttack /= PawnScript.SAManaPerAttack * 0.01f * (1 - p.SynergyDebuffs) * (1 - PawnScript.ItemBuff);
        }
        //note end of the attack
        Active = false;
    }

    //crossbow's special attack
    //crossbow's auto attacks gain damage and attack speed
    private void CrossbowSA()
    {
        //note start of the attack
        Active = true;
        //add the damage
        PawnScript.AttackSpeed += PawnScript.SAPhysicalDamage * PawnScript.SynergyBuffs;
        PawnScript.AAPhysicalDamage += PawnScript.SAMagicDamage * PawnScript.SynergyBuffs;
        PawnScript.AAMagicDamage += PawnScript.SATrueDamage * PawnScript.SynergyBuffs;
        //wait and then remove effects of the attack
        StartCoroutine(CTimer());
        //paint the crossbow orange for duration of the attack
        HealthAndManaScript.PaintCrossbow(1, 0.5f, 0, PawnScript.SADuration);
    }
 
    //remove effects of crossbow's special attack
    private IEnumerator CTimer()
    {
        //wait for the attack to end
        yield return new WaitForSeconds(PawnScript.SADuration);
        //remove the bonus damage
        PawnScript.AttackSpeed -= PawnScript.SAPhysicalDamage * PawnScript.SynergyBuffs;
        PawnScript.AAPhysicalDamage -= PawnScript.SAMagicDamage * PawnScript.SynergyBuffs;
        PawnScript.AAMagicDamage -= PawnScript.SATrueDamage * PawnScript.SynergyBuffs;
        //note end of the attack
        Active = false;
    }

    //king's special attack
    //buffs damage, mana regeneration, armor and resistance of all allies in range
    private void KingSA()
    {
        //note start of the attack
        Active = true;
        //find all nearby allies
        List<GameObject> targets = TargetingScript.SearchForNearbyAllies(PawnScript.SARange * 5.1f);
        foreach (GameObject t in targets)
        {
            Pawn p = t.GetComponent<Pawn>();

            //calculate how much the buffs will affect allies considering items, synergies etc.
            float b1 = 0.01f * PawnScript.SAManaRegen * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            float b2 = 0.01f * PawnScript.SAPhysicalDamage * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            float b3 = 0.01f * PawnScript.SAMagicDamage * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            float b4 = 0.01f * PawnScript.SAArmor * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            float b5 = 0.01f * PawnScript.SAResistance * p.SynergyBuffs * (1 + PawnScript.ItemBuff);

            p.ManaRegen *= b1;
            p.AAPhysicalDamage *= b2;
            p.AAMagicDamage *= b3;
            p.Armor *= b4;
            p.Resistance *= b5;

            //paint mana bars of affected allies yellow until the attack ends
            t.GetComponent<HealthAndMana>().PaintManaBar(1, 1, 0, PawnScript.SADuration);

            //if pawn has item, add values to the item stat tracking
            if (PawnScript.HasItem)
            {
                PawnScript.EquippedItem.StatBuff += (b1 - 0.01f * PawnScript.SAManaRegen * p.SynergyBuffs)
                    + (b2 - 0.01f * PawnScript.SAPhysicalDamage * p.SynergyBuffs)
                    + (b3 - 0.01f * PawnScript.SAMagicDamage * p.SynergyBuffs)
                    + (b4 - 0.01f * PawnScript.SAArmor * p.SynergyBuffs)
                    + (b5 - 0.01f * PawnScript.SAResistance * p.SynergyBuffs);
            }
        }
        //wait and then remove effects of special attack
        StartCoroutine(KingTimer(targets));
    }

    //removes effects of king's special attack
    private IEnumerator KingTimer(List<GameObject> targets)
    {
        //wait for the attack to end
        yield return new WaitForSeconds(PawnScript.SADuration);

        //remove effects from all affected allies
        foreach (GameObject t in targets)
        {
            Pawn p = t.GetComponent<Pawn>();
            p.ManaRegen /= 0.01f * PawnScript.SAManaRegen * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            p.AAPhysicalDamage /= 0.01f * PawnScript.SAPhysicalDamage * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            p.AAMagicDamage /= 0.01f * PawnScript.SAMagicDamage * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            p.Armor /= 0.01f * PawnScript.SAArmor * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
            p.Resistance /= 0.01f * PawnScript.SAResistance * p.SynergyBuffs * (1 + PawnScript.ItemBuff);
        }
        //note the end of the attack
        Active = false;
    }

    //linda's special attack
    //deals damage to the target linda is attacking and lower damage to all enemies near that target
    private void LindaSA()
    {
        //find all the enemies near current target
        List<GameObject> targets = TargetingScript.SearchForTargetsNearTarget(PawnScript.SARange * 5.1f);
        float phys = 0; float mag = 0; float tru = 0;
        foreach (GameObject t in targets)
        {
            //deal half of the special attack damage to them
            t.GetComponent<HealthAndMana>().TakeDamage(PawnScript, PawnScript.SAPhysicalDamage*0.5f, PawnScript.SAMagicDamage*0.5f, PawnScript.SATrueDamage*0.5f);
            phys += PawnScript.SAPhysicalDamage * 0.5f;
            mag += PawnScript.SAMagicDamage * 0.5f;
            tru += PawnScript.SATrueDamage * 0.5f;

            //perform item funcitons (if they don't have the item, value will be 0)
            float mana1 = PawnScript.SAMagicDamage * 0.5f * PawnScript.ItemMagicDmgToMana;
            HealthAndManaScript.GainMana(mana1);

            float heal1 = PawnScript.SATrueDamage * 0.5f * PawnScript.ItemTrueDamageToHealth;
            HealthAndManaScript.GainHealth(heal1);

            //add health to stat tracking
            PawnScript.GetComponent<IngameStats>().AddHealthHealed(0, heal1);

            //if pawn has item, add values to the item stat tracking
            if (PawnScript.HasItem)
            {
                PawnScript.EquippedItem.StatHealth += heal1;
                PawnScript.EquippedItem.StatMana += mana1;
            }

            //paint those targets to light blue for 0.3s
            t.GetComponent<HealthAndMana>().Paint(0.2f, 0.2f, 1, 0.3f);
        }

        //deal full damage to the current target
        TargetingScript.TargetHealthScript.TakeDamage(PawnScript, PawnScript.SAPhysicalDamage, PawnScript.SAMagicDamage, PawnScript.SATrueDamage);
        phys += PawnScript.SAPhysicalDamage;
        mag += PawnScript.SAMagicDamage;
        tru += PawnScript.SATrueDamage;

        //perform item funcitons (if they don't have the item, value will be 0)
        float mana = PawnScript.SAMagicDamage * PawnScript.ItemMagicDmgToMana;
        HealthAndManaScript.GainMana(mana);

        float heal = PawnScript.SATrueDamage * PawnScript.ItemTrueDamageToHealth;
        HealthAndManaScript.GainHealth(heal);
        //add health to stat tracking
        PawnScript.GetComponent<IngameStats>().AddHealthHealed(0, heal);
        //add all damage dealt to stat tracking
        PawnScript.GetComponent<IngameStats>().AddDamageDealt(phys, mag, tru);

        //if pawn has item, add values to the item stat tracking
        if (PawnScript.HasItem)
        {
            PawnScript.EquippedItem.StatHealth += heal;
            PawnScript.EquippedItem.StatMana += mana;
        }

        //paint the target to blue for 0.3s
        TargetingScript.TargetHealthScript.Paint(0, 0, 1, 0.3f);
    }

    //mace's special attack
    //heals, gains armor and resistance that are removed at the end of the round
    private void MaceSA()
    {
        //calculate the amount of armor and resistance considering items
        float a = PawnScript.SAArmor * (PawnScript.ItemBuff + 1);
        float r = PawnScript.SAResistance * (PawnScript.ItemBuff + 1);
        //add armor and resistance
        PawnScript.BonusArmor += a;
        PawnScript.BonusResistance += r;

        //calculate the heal considering items
        float heal = PawnScript.SAHeal * (PawnScript.ItemBuff + 1);
        //heal the pawn
        HealthAndManaScript.GainHealth(heal);
        //add the heal to stat tracking
        PawnScript.GetComponent<IngameStats>().AddHealthHealed(heal, 0);

        //if pawn has item, add the value to the item stat tracking
        if (PawnScript.HasItem)
        {
            PawnScript.EquippedItem.StatBuff += (a - PawnScript.SAArmor) + (r - PawnScript.SAResistance) + (heal - PawnScript.SAHeal);
        }

        //paint the Mace brown for 0.3s
        HealthAndManaScript.Paint(0.38f, 0.18f, 0.11f, 0.3f);
    }

    //sword's special attack
    //deals true damage to the target it's currently attacking
    private void SwordSA()
    {
        //deal damage to the current target
        TargetingScript.TargetHealthScript.TakeDamage(PawnScript, PawnScript.SAPhysicalDamage, PawnScript.SAMagicDamage, PawnScript.SATrueDamage);

        //perform item funcitons (if they don't have the item, value will be 0)
        float mana = PawnScript.SAMagicDamage * PawnScript.ItemMagicDmgToMana;
        HealthAndManaScript.GainMana(mana);

        float heal = PawnScript.SATrueDamage * PawnScript.ItemTrueDamageToHealth;
        HealthAndManaScript.GainHealth(heal);
        PawnScript.GetComponent<IngameStats>().AddHealthHealed(0, heal);

        //if pawn has item, add values to the item stat tracking
        if (PawnScript.HasItem)
        {
            PawnScript.EquippedItem.StatHealth += heal;
            PawnScript.EquippedItem.StatMana += mana;
        }
        //add damage to stat tracking
        PawnScript.GetComponent<IngameStats>().AddDamageDealt(PawnScript.SAPhysicalDamage, PawnScript.SAMagicDamage, PawnScript.SATrueDamage);

        //paint the target pink for 0.3s
        TargetingScript.TargetHealthScript.Paint(1, 0.18f, 0.78f, 0.3f);

    }

    //teeny's special attack
    //heals the ally with lowest health (including self)
    private void TeenySA()
    {
        //find ally with lowest health
        HealthAndMana target = TargetingScript.SearchForAllyWithLeastHealth();
        if (target)
        {
            //calculate actual value of the heal considering items
            float heal = PawnScript.SAHeal * (PawnScript.ItemBuff + 1);
            //heal the ally
            target.GainHealth(heal);
            //add the heal to stat tracking
            PawnScript.GetComponent<IngameStats>().AddHealthHealed(heal, 0);
            //paint the ally green for 0.3s
            target.Paint(0, 1, 0, 0.3f);

            //if pawn has item, add the value to the item stat tracking
            if (PawnScript.HasItem)
                PawnScript.EquippedItem.StatBuff += heal - PawnScript.SAHeal;
        }
    }
    #endregion
}
