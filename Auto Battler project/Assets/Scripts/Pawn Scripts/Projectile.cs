﻿using UnityEngine;

/// <summary>
/// This script will be on any projectile prefab that is spawned
/// by our ranged pawns.
/// </summary>

namespace AutoBattles
{
    public class Projectile : MonoBehaviour
    {
        #region Variables
        private bool _isReady;
        private float _critical;

        [SerializeField]
        [Tooltip("How fast the projectile will travel to its target. If 0 at runtime it will default to 25.")]
        private float _projectileSpeed;

        //references
        private Transform _targetTransform;
        private HealthAndMana _targetHealthScript;
        private Pawn _pawnScript;
        #endregion

        #region Properties
        protected bool IsReady { get => _isReady; set => _isReady = value; }
        protected float ProjectileSpeed { get => _projectileSpeed; set => _projectileSpeed = value; }
        public float Critical { get => _critical; set => _critical = value; }
        //references
        protected Transform TargetTransform { get => _targetTransform; set => _targetTransform = value; }
        protected HealthAndMana TargetHealthScript { get => _targetHealthScript; set => _targetHealthScript = value; }
        public Pawn PawnScript { get => _pawnScript; set => _pawnScript = value; }

        #endregion

        #region Methods
        protected virtual void Awake()
        {
            if (ProjectileSpeed == 0)
                ProjectileSpeed = 25f;
        }

        //this is called by the pawn who is instantiating us
        public virtual void Setup(Transform targetTransform, HealthAndMana targetHealthScript, Pawn pawnScript, float critical)
        {
            TargetTransform = targetTransform;
            TargetHealthScript = targetHealthScript;
            PawnScript = pawnScript;
            Critical = critical;

            IsReady = true;
        }

        protected virtual void Update()
        {
            //if our target is deleted before this reaches them
            //delete this gameobject and return
            if (TargetTransform == null)
            {
                Destroy(gameObject);
                return;
            }

            if (IsReady)
            {
                transform.position = Vector3.MoveTowards(transform.position, TargetTransform.position, ProjectileSpeed * Time.deltaTime);

                float distance = Vector3.Distance(transform.position, TargetTransform.position);

                if (distance <= 0.02f)
                {
                    //we hit the target
                    DealDamageAndDestruct();
                }
            }
        }

        protected virtual void DealDamageAndDestruct()
        {
            //calculate physical damage inclucding critical chance from item
            float PhysDmg = PawnScript.AAPhysicalDamage + Critical * 0.01f * PawnScript.AAPhysicalDamage;
            TargetHealthScript.TakeDamage(PawnScript, PhysDmg, PawnScript.AAMagicDamage, PawnScript.AATrueDamage);

            //calculate values coming from items (if the pawn doesn't have these items, the value will be 0)
            float mana = PawnScript.AAMagicDamage * PawnScript.ItemMagicDmgToMana;
            PawnScript.GetComponent<HealthAndMana>().GainMana(mana);

            float heal = PawnScript.AATrueDamage * PawnScript.ItemTrueDamageToHealth;
            PawnScript.GetComponent<HealthAndMana>().GainHealth(heal);
            PawnScript.GetComponent<IngameStats>().AddHealthHealed(0, heal);

            //if the pawn has items, add values to item's stat tracking
            if (PawnScript.HasItem)
            {
                PawnScript.EquippedItem.StatHealth += heal;
                PawnScript.EquippedItem.StatMana += mana;
            }

            //destroy the projectile
            Destroy(gameObject);
        }
        #endregion
    }
}

