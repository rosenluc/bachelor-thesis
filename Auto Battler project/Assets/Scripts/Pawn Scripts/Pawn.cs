﻿using UnityEngine;

namespace AutoBattles
{
    /// <summary>
    /// Statistics specific for each created pawn (as in game object)
    /// </summary>
    [RequireComponent(typeof(Targeting), typeof(AutoAttack), typeof(Movement))]
    [RequireComponent(typeof(Status), typeof(HomeBase), typeof(HealthAndMana))]
    [RequireComponent(typeof(SpecialAttack))]
    public class Pawn : MonoBehaviour
    {
        #region Variables

        [Header("BASE STATS")]
        [SerializeField]
        [Tooltip("Requires a PawnStats scriptable object.")]
        private PawnStats _stats;

        [Header("ATTACK")]
        [Header("DO NOT ALTER IN EDITOR!")]
        [Header("Read Only - Calculated Stats")]
        [Header("AUTO ATTACK")]
        [SerializeField]
        [Tooltip("Physical damage a pawn does on an auto attack.")]
        private float _AAPhysicalDamage;
        [SerializeField]
        [Tooltip("Magic damage a pawn does on an auto attack.")]
        private float _AAMagicDamage;
        [SerializeField]
        [Tooltip("True damage a pawn does on an auto attack.")]
        private float _AATrueDamage;
        [SerializeField]
        [Tooltip("The range of the pawns auto attacks (in squares, i.e. 1 = attack 1 tile away, 2 = attack 2 tiles away")]
        private float _AARange;
        [SerializeField]
        [Tooltip("Pawn waits for 100/(100 + attack speed) seconds between attacks.")]
        private float _attackSpeed;

        [Header("SPECIAL ATTACK")]
        [SerializeField]
        [Tooltip("Physical damage a pawn does on a special attack.")]
        private float _SAPhysicalDamage;
        [SerializeField]
        [Tooltip("Magic damage a pawn does on a special attack.")]
        private float _SAMagicDamage;
        [SerializeField]
        [Tooltip("True damage a pawn does on a special attack.")]
        private float _SATrueDamage;
        [SerializeField]
        [Tooltip("Bonus attack speed granted by special attack.")]
        private float _SAttackSpeed;
        [SerializeField]
        [Tooltip("Healing granted by special attack.")]
        private float _SAHeal;
        [SerializeField]
        [Tooltip("How long buff from SA lasts.")]
        private float _SADuration;
        [SerializeField]
        [Tooltip("The range of the pawns special attack (in squares, i.e. 1 = attack 1 tile away, 2 = attack 2 tiles away")]
        private float _SARange;
        [SerializeField]
        [Tooltip("Bonus mana regenaration granted by special attack.")]
        private float _SAManaRegen;
        [SerializeField]
        [Tooltip("Bonus mana per attack granted by special attack.")]
        private float _SAManaPerAttack;
        [SerializeField]
        [Tooltip("Bonus armor granted by special attack.")]
        private float _SAArmor;
        [SerializeField]
        [Tooltip("Bonus resistance granted by special attack.")]
        private float _SAResistance;

        [Header("OTHER STATS")]
        [Header("DO NOT ALTER IN EDITOR!")]
        [Header("Read Only - Calculated Stats")]
        [SerializeField]
        [Tooltip("The amount of mana a pawn will generate on each auto attack.")]
        private float _manaPerAttack;
        [SerializeField]
        [Tooltip("The amount of mana a pawn will generate on every second.")]
        private float _manaRegen;
        [SerializeField]
        [Tooltip("How fast the pawn can move around the chess board.")]
        private float _moveSpeed;

        [Header("DEF")]
        [Header("DO NOT ALTER IN EDITOR!")]
        [Header("Read Only - Calculated Stats")]
        [SerializeField]
        [Tooltip("The total amount of damage a pawn can take before dying.")]
        private float _health;
        [SerializeField]
        [Tooltip("The total amount of mana a pawn needs to accumulate before being able to cast the special attack.")]
        private float _mana;
        [SerializeField]
        [Tooltip("Total armor used in calculating physical damage reduction.")]
        private float _armor;
        [SerializeField]
        [Tooltip("Total resistance used in calculating magical damage reduction.")]
        private float _resistance;
        [SerializeField]
        [Tooltip("Amount of health pawn regenerates every second.")]
        private float _healing;

        // These stats are gained from Mace's special attack
        // Must be separate so that we can remove it at the end ofround
        [Header("BONUS STATS")]        
        [Header("DO NOT ALTER IN EDITOR!")]
        [Header("Read Only - Calculated Stats")]
        [SerializeField]
        [Tooltip("The bonus armor received.")]
        private float _bonusArmor;
        [SerializeField]
        [Tooltip("The bonus resistance received.")]
        private float _bonusResistance;

        // Stats gained from synergies
        [Header("EXACT SYNERGY BONUS AMOUNTS")]
        [Header("DO NOT ALTER IN EDITOR!")]
        [Header("Read Only - Calculated Stats")]
        [SerializeField]
        private float _synergyPhysicalDamage;
        [SerializeField]
        private float _synergyMagicDamage;
        [SerializeField]
        private float _synergyTrueDamage;
        [SerializeField]
        private float _synergyHealth;
        [SerializeField]
        private float _synergyHealing;
        [SerializeField]
        private float _synergyArmor;
        [SerializeField]
        private float _synergyResistance;
        [SerializeField]
        private float _synergyGold;
        [SerializeField]
        private float _synergyBuffs;
        [SerializeField]
        private float synergyDebuffs;

        // Stats gained from items
        [Header("ITEM BONUS AMOUNTS")]
        [Header("DO NOT ALTER IN EDITOR!")]
        [Header("Read Only - Calculated Stats")]
        [SerializeField]
        private float _itemBuff;
        [SerializeField]
        private float _itemMagicDmgToMana;
        [SerializeField]
        private float _itemHealing;
        [SerializeField]
        private float _itemTrueDmgMitigation;
        [SerializeField]
        private float _itemArmorPenetration;
        [SerializeField]
        private float _itemResistancePenetration;
        [SerializeField]
        private float _itemPhysicalMaxCrit;
        [SerializeField]
        private float _itemArmor;
        [SerializeField]
        private float _itemResistance;
        [SerializeField]
        private float _itemTrueDamageToHealth;

        //true if the pawn has an item equipped
        [SerializeField]
        private bool _hasItem;

        //script of the equipped item
        private Item _equippedItem;
        //if the equipped item is an emblem, which trait is added
        private string _extraOrigin;

        #endregion

        #region Properties
        //This will hold a scriptable object 
        //containing all data for the base stats of the pawn
        public PawnStats Stats { get => _stats; set => _stats = value; }
        public float AAPhysicalDamage { get => _AAPhysicalDamage; set => _AAPhysicalDamage = value; }
        public float AAMagicDamage { get => _AAMagicDamage; set => _AAMagicDamage = value; }
        public float AATrueDamage { get => _AATrueDamage; set => _AATrueDamage = value; }
        public float AARange { get => _AARange; set => _AARange = value; }
        public float AttackSpeed { get => _attackSpeed; set => _attackSpeed = value; }

        public float SAPhysicalDamage { get => _SAPhysicalDamage; set => _SAPhysicalDamage = value; }
        public float SAMagicDamage { get => _SAMagicDamage; set => _SAMagicDamage = value; }
        public float SATrueDamage { get => _SATrueDamage; set => _SATrueDamage = value; }
        public float SAttackSpeed { get => _SAttackSpeed; set => _SAttackSpeed = value; }
        public float SAHeal { get => _SAHeal; set => _SAHeal = value; }
        public float SADuration { get => _SADuration; set => _SADuration = value; }
        public float SARange { get => _SARange; set => _SARange = value; }
        public float SAManaRegen { get => _SAManaRegen; set => _SAManaRegen = value; }
        public float SAManaPerAttack { get => _SAManaPerAttack; set => _SAManaPerAttack = value; }
        public float SAArmor { get => _SAArmor; set => _SAArmor = value; }
        public float SAResistance { get => _SAResistance; set => _SAResistance = value; }

        public float ManaPerAttack { get => _manaPerAttack; set => _manaPerAttack = value; }
        public float ManaRegen { get => _manaRegen; set => _manaRegen = value; }
        public float MoveSpeed { get => _moveSpeed; set => _moveSpeed = value; }

        public float Health { get => _health; set => _health = value; }
        public float Mana { get => _mana; set => _mana = value; }
        public float Armor { get => _armor; set => _armor = value; }
        public float Resistance { get => _resistance; set => _resistance = value; }
        public float Healing { get => _healing; set => _healing = value; }

        public float BonusArmor { get => _bonusArmor; set => _bonusArmor = value; }
        public float BonusResistance { get => _bonusResistance; set => _bonusResistance = value; }

        public float SynergyPhysicalDamage { get => _synergyPhysicalDamage; set => _synergyPhysicalDamage = value; }
        public float SynergyMagicDamage { get => _synergyMagicDamage; set => _synergyMagicDamage = value; }
        public float SynergyTrueDamage { get => _synergyTrueDamage; set => _synergyTrueDamage = value; }
        public float SynergyHealth { get => _synergyHealth; set => _synergyHealth = value; }
        public float SynergyHealing { get => _synergyHealing; set => _synergyHealing = value; }
        public float SynergyArmor { get => _synergyArmor; set => _synergyArmor = value; }
        public float SynergyResistance { get => _synergyResistance; set => _synergyResistance = value; }
        public float SynergyGold { get => _synergyGold; set => _synergyGold = value; }
        public float SynergyBuffs { get => _synergyBuffs; set => _synergyBuffs = value; }
        public float SynergyDebuffs { get => synergyDebuffs; set => synergyDebuffs = value; }

        public Item EquippedItem { get => _equippedItem; set => _equippedItem = value; }
        public string ExtraOrigin { get => _extraOrigin; set => _extraOrigin = value; }

        public float ItemBuff { get => _itemBuff; set => _itemBuff = value; }
        public float ItemMagicDmgToMana { get => _itemMagicDmgToMana; set => _itemMagicDmgToMana = value; }
        public float ItemHealing { get => _itemHealing; set => _itemHealing = value; }
        public float ItemTrueDmgMitigation { get => _itemTrueDmgMitigation; set => _itemTrueDmgMitigation = value; }
        public float ItemArmorPenetration { get => _itemArmorPenetration; set => _itemArmorPenetration = value; }
        public float ItemResistancePenetration { get => _itemResistancePenetration; set => _itemResistancePenetration = value; }
        public float ItemPhysicalMaxCrit { get => _itemPhysicalMaxCrit; set => _itemPhysicalMaxCrit = value; }
        public float ItemArmor { get => _itemArmor; set => _itemArmor = value; }
        public float ItemResistance { get => _itemResistance; set => _itemResistance = value; }
        public float ItemTrueDamageToHealth { get => _itemTrueDamageToHealth; set => _itemTrueDamageToHealth = value; }

        public bool HasItem { get => _hasItem; set => _hasItem = value; }

        #endregion

        #region Methods
        protected virtual void Awake()
        {
            InitialSetup();
        }

        //Removes the bonus stats we received from synergies 
        public virtual void ClearSynergyBonuses()
        {
            //warrior
            AAPhysicalDamage -= SynergyPhysicalDamage;
            AAMagicDamage -= SynergyMagicDamage;
            AATrueDamage -= SynergyTrueDamage;

            SAPhysicalDamage -= SynergyPhysicalDamage;
            SAMagicDamage -= SynergyMagicDamage;
            SATrueDamage -= SynergyTrueDamage;

            SynergyPhysicalDamage = 0;
            SynergyMagicDamage = 0;
            SynergyTrueDamage = 0;

            //orc
            Healing -= SynergyHealing;
            Armor -= SynergyArmor;
            Resistance -= SynergyResistance;

            SynergyHealing = 0;
            SynergyArmor = 0;
            SynergyResistance = 0;

            //knight
            SynergyBuffs = 1;
            SynergyDebuffs = 1;

            //human
            Health -= SynergyHealth;
            SynergyHealth = 0;
            SynergyGold = 0;
        }

        //True if the pawn does not have the trait that is added by the item
        //E. g. if the item is Human Emblem and the pawn is King, it returns false
        //since king already has Human trait
        public bool EmblemEquippable(GameObject item)
        {
            switch (item.name)
            {
                case "Orc Emblem":
                    if (Stats.origins.Contains(PawnStats.Origin.Orc))
                    {
                        return false;
                    }
                    return true;
                case "Human Emblem":
                    if (Stats.origins.Contains(PawnStats.Origin.Human))
                    {
                        return false;
                    }
                    return true;
            }
            return true;
        }

        //Equip pawn with the item
        public void Equip(Item item)
        {
            HasItem = true;
            //reset tracked statistics of the item to 0 in case it was equipped by different pawn before
            item.ResetStats();

            //change the required statistic based on the item nature
            switch (item.name)
            {
                case "Orc Emblem":
                    ExtraOrigin = PawnStats.Origin.Orc.ToString();
                    break;
                case "Human Emblem":
                    ExtraOrigin = PawnStats.Origin.Human.ToString();
                    break;
                case "Amulet of Mara":
                    ItemBuff = item.Stats.Health;
                    break;
                case "Birch Wand":
                    ItemMagicDmgToMana = item.Stats.MagicDamage;
                    break;
                case "Coat of Healing":
                    ItemHealing = item.Stats.Health;
                    break;
                case "Doran's Ring":
                    ManaRegen += item.Stats.ManaRegen;
                    break;
                case "Dwarven Helmet":
                    ItemTrueDmgMitigation = item.Stats.TrueDamage;
                    break;
                case "Morgul Blade":
                    ItemArmorPenetration = item.Stats.Armor;
                    ItemResistancePenetration = item.Stats.Resistance;
                    break;
                case "Precision Arrow":
                    ItemPhysicalMaxCrit = item.Stats.PhysicalDamage;
                    break;
                case "Sunlight Shield":
                    ItemArmor = item.Stats.Armor;
                    ItemResistance = item.Stats.Resistance;
                    break;
                case "Viper Sword":
                    ItemTrueDamageToHealth = item.Stats.TrueDamage;
                    break;
                default:
                    Debug.LogError("Item called " + item.name + " not found.");
                    break;
            }
            EquippedItem = item;
        }

        //Generates the special attack description with accurate values
        public string CalculateDescription()
        {
            string result = "";
            switch (Stats.name.Substring(0, 2))
            {
                case "As":
                    float phys = SAPhysicalDamage + SynergyPhysicalDamage;
                    float mag = SAMagicDamage + SynergyMagicDamage;
                    result = $"Deals {phys} and {mag} damage to enemy with most damage dealt so far in this round.";
                    break;
                case "Co":
                    float reg = SAManaRegen * (ItemBuff + 1);
                    float pa = SAManaPerAttack * (ItemBuff + 1);
                    result = $"Lowers mana regeneration by {reg}% and mana per attack by {pa}% of enemies in range of {SARange} for a {SADuration}s.";
                    break;
                case "Cr":
                    float asp = SAttackSpeed * (ItemBuff + 1);
                    float physs = SAPhysicalDamage + SynergyPhysicalDamage;
                    float magg = SAMagicDamage + SynergyMagicDamage;
                    result = $"Unit gains {asp} attack speed, {physs} physical and {magg} magic damage for {SADuration}s.";
                    break;
                case "Ki":
                    float regg = SAManaRegen * (ItemBuff + 1);
                    float dmg = SAMagicDamage * (ItemBuff + 1);
                    float def = SAArmor * (ItemBuff + 1);
                    result = $"Buffs mana regeneration by {regg}%, magical and physical damage by {dmg}%, armour, and resistance by {def}% of allies in {SARange} range for {SADuration}s.";
                    break;
                case "Li":
                    float maggg = SAMagicDamage + SynergyMagicDamage;
                    result = $"Deals {maggg} magic damage to the target and 50% of that magic damage to enemies in range {SARange} from the target.";
                    break;
                case "Ma":
                    float heal = SAHeal * (ItemBuff + 1);
                    float armor = SAArmor * (ItemBuff + 1);
                    float res = SAResistance * (ItemBuff + 1);
                    result = $"Heals {heal} health, gains {armor} armour and {res} resistance that stack until the end of the round.";
                    break;
                case "Sw":
                    float tru = SATrueDamage + SynergyTrueDamage;
                    result = $"Deals {tru} true damage to the target.";
                    break;
                case "Te":
                    float he = SAHeal * (ItemBuff + 1);
                    result = $"Heals {he} healt points of ally with lowest current health.";
                    break;
            }
            return result;
        }

        //Sets all values of the pawn to the default
        public void InitialSetup()
        {
            name = Stats.name;
            AAPhysicalDamage = Stats.AAPhysicalDamage;
            AAMagicDamage = Stats.AAMagicDamage;
            AATrueDamage = Stats.AATrueDamage;
            AARange = Stats.AARange;
            AttackSpeed = Stats.attackSpeed;

            SAPhysicalDamage = Stats.SAPhysicalDamage;
            SAMagicDamage = Stats.SAMagicDamage;
            SATrueDamage = Stats.SATrueDamage;
            SAttackSpeed = Stats.SAttackSpeed;
            SAHeal = Stats.SAHeal;
            SADuration = Stats.SADuration;
            SARange = Stats.SARange;
            SAManaRegen = Stats.SAManaRegen;
            SAManaPerAttack = Stats.SAManaPerAttack;
            SAArmor = Stats.SAArmor;
            SAResistance = Stats.SAResistance;

            ManaPerAttack = Stats.manaPerAttack;
            ManaRegen = Stats.manaRegen;
            MoveSpeed = Stats.moveSpeed;

            Health = Stats.health;
            Mana = Stats.mana;
            Armor = Stats.armor;
            Resistance = Stats.resistance;
            Healing = Stats.healing;

            BonusArmor = 0;
            BonusResistance = 0;

            SynergyPhysicalDamage = 0;
            SynergyMagicDamage = 0;
            SynergyTrueDamage = 0;
            SynergyHealing = 0;
            SynergyArmor = 0;
            SynergyResistance = 0;
            SynergyBuffs = 1;
            SynergyDebuffs = 1;
            SynergyHealth = 0;
            SynergyGold = 0;

            HasItem = false;
        }

        //Clears bonus armor and resistance
        public void ClearBonusesAfterRound()
        {
            BonusArmor = 0;
            BonusResistance = 0;
        }
        #endregion
    }

}
