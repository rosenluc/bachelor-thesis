using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AutoBattles
{
    /// <summary>
    /// This class holds specific values for each created item (item game object)
    /// </summary>
    public class Item : MonoBehaviour
    {
        #region Variables
        private ItemStats _stats;
        private string _name;
        private string _description;

        private float _statBuff;
        private float _statMana;
        private float _statHealth;
        private float _statDmgReduced;
        private float _statPhysicalDmgPenetrated;
        private float _statMagicDmgPenetrated;
        private float _statBonusPhysicalDmg;
        private float _statBonusArmor;
        private float _statBonusResist;

        private float _statRatio;
        #endregion

        #region Properties
        //reference to default item values
        public ItemStats Stats { get => _stats; set => _stats = value; }
        public string Name { get => _name; set => _name = value; }
        public string Description { get => _description; set => _description = value; }

        //values this item provides
        public float StatBuff { get => _statBuff; set => _statBuff = value; }
        public float StatMana { get => _statMana; set => _statMana = value; }
        public float StatHealth { get => _statHealth; set => _statHealth = value; }
        public float StatDmgReduced { get => _statDmgReduced; set => _statDmgReduced = value; }
        public float StatPhysicalDmgPenetrated { get => _statPhysicalDmgPenetrated; set => _statPhysicalDmgPenetrated = value; }
        public float StatMagicDmgPenetrated { get => _statMagicDmgPenetrated; set => _statMagicDmgPenetrated = value; }
        public float StatBonusPhysicalDmg { get => _statBonusPhysicalDmg; set => _statBonusPhysicalDmg = value; }
        public float StatBonusArmor { get => _statBonusArmor; set => _statBonusArmor = value; }
        public float StatBonusResist { get => _statBonusResist; set => _statBonusResist = value; }

        //value that helps with scaling the stat bar
        public float StatRatio { get => _statRatio; set => _statRatio = value; }
        #endregion

        #region Methods

        //loads default values
        public void Setup(ItemStats s)
        {
            Name = s.name;
            Stats = s;
            Description = s.Description;
        }

        //resets all statistics and the ratio
        public void ResetStats()
        {
            StatBuff = 0;
            StatMana = 0;
            StatHealth = 0;
            StatDmgReduced = 0;
            StatPhysicalDmgPenetrated = 0;
            StatMagicDmgPenetrated = 0;
            StatBonusPhysicalDmg = 0;
            StatBonusArmor = 0;
            StatBonusResist = 0;
            StatRatio = 5;
        }

        //returns the statistics that are changed by this item
        //these are stats that are tracked and visualized to the player
        public Tuple<float, float> ChangingStats()
        {
            switch (Name)
            {
                case "Amulet of Mara":
                    return Tuple.Create(StatBuff, 0f);
                case "Birch Wand":
                    return Tuple.Create(StatMana, 0f);
                case "Coat of Healing":
                    return Tuple.Create(StatHealth, 0f);
                case "Doran's Ring":
                    return Tuple.Create(StatMana, 0f);
                case "Dwarven Helmet":
                    return Tuple.Create(StatDmgReduced, 0f);
                case "Morgul Blade":
                    return Tuple.Create(StatPhysicalDmgPenetrated, StatMagicDmgPenetrated);
                case "Precision Arrow":
                    return Tuple.Create(StatBonusPhysicalDmg, 0f);
                case "Sunlight Shield":
                    return Tuple.Create(StatBonusArmor, StatBonusResist);
                case "Viper Sword":
                    return Tuple.Create(StatHealth, 0f);
            }
            return Tuple.Create(0f, 0f);
        }
        #endregion
    }
}
