using UnityEngine;
using UnityEngine.EventSystems;

namespace AutoBattles
{
    /// <summary>
    /// This script takes care of the draggable game objects that represent items
    /// </summary>
    public class ItemDragAndDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
    {
        #region Variables
        private RectTransform _rTransform;
        private CanvasGroup _cGroup;
        private Vector3 _startingPosition;
        private GameObject _hoveredPawn;
        private bool _isDragged;
        private bool _isEquipped;
        //references
        private GameManager _gameManager;
        #endregion

        #region Properties
        //reference to the game object transform
        public RectTransform RTransform { get => _rTransform; set => _rTransform = value; }
        //reference to the canvas group
        public CanvasGroup CGroup { get => _cGroup; set => _cGroup = value; }
        //position of the item before player started dragging it
        public Vector3 StartingPosition { get => _startingPosition; set => _startingPosition = value; }
        //reference to the pawn the item is hovering on
        public GameObject HoveredPawn { get => _hoveredPawn; set => _hoveredPawn = value; }
        //true if player is dragging the item
        public bool IsDragged { get => _isDragged; set => _isDragged = value; }
        //true if the item belongs to a pawn
        public bool IsEquipped { get => _isEquipped; set => _isEquipped = value; }
        //references
        public GameManager GameManager { get => _gameManager; set => _gameManager = value; }
        #endregion

        #region Methods
        private void Awake()
        {
            RTransform = GetComponent<RectTransform>();
            CGroup = GetComponent<CanvasGroup>();
            IsDragged = false;
            IsEquipped = false;
            GameManager = GameManager.Instance;
            if (!GameManager)
            {
                Debug.LogError("No GameManager singleton instance found in the scene. Please add an GameManager script to the game manager gameobject " +
                    "before entering playmode!");
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            //if item is equipped, it cannot be dragged
            if (IsEquipped)
            {
                eventData.pointerDrag = null;
                return;
            }
            IsDragged = true;
            //set the starting position to the current position
            StartingPosition = RTransform.position;
            //make item a little transparent while being dragged
            CGroup.alpha = 0.6f;
        }

        public void OnDrag(PointerEventData eventData)
        {
            Canvas canvas = GetComponentInParent<Canvas>();
            //move the object along with cursor
            RTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            IsDragged = false;
            //make item fully opaque
            CGroup.alpha = 1;
            //if item isn't placed over a pawn or if the pawn already has an item
            //return the item back to inventory (snap it back to starting position)
            if(HoveredPawn == null || HoveredPawn.GetComponent<Pawn>().HasItem)
            {
                RTransform.position = StartingPosition;
            }
            else
            {
                //check if the pawn can be equipped with dragged item
                if (HoveredPawn.GetComponent<Pawn>().EmblemEquippable(gameObject))
                {
                    if(gameObject.name == "Orc Emblem" || gameObject.name == "Human Emblem")
                    {
                        //add the trait to the synergy
                        //to avoid doubling pawn's effect, we remove it from synergy
                        //then equip it, which adds a trait and add it to synergy again
                        GameManager.GetComponent<SynergyManager>().PawnLost(HoveredPawn.GetComponent<Pawn>());
                        HoveredPawn.GetComponent<Pawn>().Equip(gameObject.GetComponent<Item>());
                        GameManager.GetComponent<SynergyManager>().PawnAcquired(HoveredPawn.GetComponent<Pawn>());

                    }
                    else
                    {
                        //equip the item
                        HoveredPawn.GetComponent<Pawn>().Equip(gameObject.GetComponent<Item>());
                    }
                    //set the item to hover above pawn's healthbar and move with it
                    transform.SetParent(HoveredPawn.GetComponent<HealthAndMana>().HealthBarTransform);
                    RTransform.localPosition = new Vector3(18.8f, 9.5f, 0);
                    //scale the item down when equipped
                    RTransform.localScale *= 0.5f;
                    IsEquipped = true;
                }
                else
                {
                    //if the item is unequipabble, snap it back to inventory
                    RTransform.position = StartingPosition;
                }
            }
        }

        //makes item available again for equipping
        //this function is called after a pawn with item is sold or upgraded
        public void EnableItem()
        {
            IsEquipped = false;
            RTransform.localScale *= 2;
        }

        //show tooltip with item description when cursor hovers over it
        public void OnPointerExit(PointerEventData eventData)
        {
            GameManager.GetComponent<UserInterfaceManager>().CloseDesktopTooltip();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            GameManager.GetComponent<UserInterfaceManager>().OpenDesktopTooltip(null, gameObject.GetComponent<Item>());
        }
        #endregion
    }
}
