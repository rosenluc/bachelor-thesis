using System.Collections.Generic;
using UnityEngine;

namespace AutoBattles
{
    public class ItemDatabase : Singleton<ItemDatabase>
    {
        [SerializeField]
        private List<ItemStats> _items;

        public List<ItemStats> Items { get => _items; protected set => _items = value; }
    }
}
