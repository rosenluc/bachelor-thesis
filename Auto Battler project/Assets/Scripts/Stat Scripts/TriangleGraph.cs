using UnityEngine;
using UnityEngine.UI;

namespace AutoBattles
{
    /// <summary>
    /// This script creates a triangular graph with a triangular line inside
    /// </summary>
    /// 
    public class TriangleGraph : MonoBehaviour
    {
        #region Variables
        private RectTransform _graph;
        //whether the graph has already been calculated
        private bool initialized;

        //positions of the triangle vertices and it's middle point (the triangle is equilateral)
        private Vector2 pointA;
        private Vector2 pointB;
        private Vector2 pointC;
        private Vector2 pointMid;
        #endregion

        #region Properties
        public RectTransform Graph { get => _graph; set => _graph = value; }
        #endregion

        #region Methods
        private void Awake()
        {
            Graph = GetComponent<RectTransform>();
            //create the outline of the graph
            DrawTriangle();
            HideGraph();
            initialized = false;
        }

        //connects two points with a line
        private void ConnectPoints(Vector2 pointA, Vector2 pointB, Color color, string name)
        {
            //create a line
            GameObject line = new GameObject(name, typeof(Image));
            line.transform.SetParent(Graph);
            line.GetComponent<Image>().color = color;

            //calculate the angle and length of the line
            Vector2 direction = (pointB - pointA).normalized;
            float distance = Vector2.Distance(pointA, pointB);
            RectTransform rTrans = line.GetComponent<RectTransform>();

            //set the line accordingly
            rTrans.localScale = new Vector3(1, 1, 1);
            rTrans.sizeDelta = new Vector2(distance, 1f);
            rTrans.anchorMin = new Vector2(0, 0);
            rTrans.anchorMax = new Vector2(0, 0);
            rTrans.localEulerAngles = new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI);
            rTrans.anchoredPosition = pointA + direction * distance * 0.5f;
        }

        //creates the outline triangle
        private void DrawTriangle()
        {
            //get width and height of the graph
            float graphWidth = Graph.sizeDelta.x;
            float graphHeight = Graph.sizeDelta.y;

            //calculate outline vertices and middle point
            pointA = new Vector2(0, graphHeight);
            pointB = new Vector2(graphWidth, graphHeight);
            pointC = new Vector2(graphWidth * 0.5f, 0);
            pointMid = new Vector2(graphWidth * 0.5f, graphHeight*2/3);

            Color white = new Color(1, 1, 1);
            Color whiteSemi = new Color(1, 1, 1, 0.5f);

            //connect the outline vertices with a white line
            ConnectPoints(pointA, pointB, white, "outline");
            ConnectPoints(pointB, pointC, white, "outline");
            ConnectPoints(pointC, pointA, white, "outline");
            //connect outline vertices with the middle point with a semi transparent white line
            ConnectPoints(pointA, pointMid, whiteSemi, "inline");
            ConnectPoints(pointB, pointMid, whiteSemi, "inline");
            ConnectPoints(pointC, pointMid, whiteSemi, "inline");
        }

        //hide the graph when it's not supposed to be displayed
        public void HideGraph()
        {
            gameObject.SetActive(false);
        }

        //show graph
        //if it wasn't created yet, create it
        public void ShowGraph(float val1, float val2, float val3)
        {
            if (!initialized)
            {
                CreateGraph(val1, val2, val3);
            }
            gameObject.SetActive(true);
        }

        //destroy the graph
        //the outline does not have to be destroyed since it doesn't change with input values
        public void DestroyGraph()
        {
            //outline is first 10 children (background, 3 labels, 3 outer lines, 3 inner lines)
            for (int i = 10; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            initialized = false;
            gameObject.SetActive(false);
        }

        //create the graph (the part that changes with values)
        private void CreateGraph(float val1, float val2, float val3)
        {
            float maxLength = Vector2.Distance(pointA, pointMid);
            //find the biggest value
            float maxValue = Mathf.Max(Mathf.Max(val1, val2), val3);

            //calculate position of each value so that they all fit into the graph
            //and they are equally scaled
            float distance = (val1 / maxValue) * maxLength;
            Vector2 pointXP = pointMid + new Vector2(-distance * Mathf.Sin(Mathf.PI * 60 / 180.0f), distance * Mathf.Sin(Mathf.PI * 30 / 180.0f));

            distance = (val2 / maxValue) * maxLength;
            Vector2 pointRoll = pointMid + new Vector2(distance * Mathf.Sin(Mathf.PI * 60 / 180.0f), distance * Mathf.Sin(Mathf.PI * 30 / 180.0f));

            distance = (val3 / maxValue) * maxLength;
            Vector2 pointPawn = pointMid + new Vector2(0, -distance);

            Color blue = new Color(0.5f, 0.5f, 1);
            //connect the value points with a blue line
            ConnectPoints(pointXP, pointRoll, blue, "line");
            ConnectPoints(pointRoll, pointPawn, blue, "line");
            ConnectPoints(pointPawn, pointXP, blue, "line");
        }
        #endregion
    }
}
