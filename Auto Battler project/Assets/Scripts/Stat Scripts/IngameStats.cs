using UnityEngine;

namespace AutoBattles
{
    /// <summary>
    /// This script tracks respective statistics of each pawn
    /// </summary>
    public class IngameStats : MonoBehaviour
    {
        #region Variables
        [Header("Damage Dealt Stats")]
        [Header("All Damage")]
        [SerializeField]
        private float _allDamageDealt;
        [SerializeField]
        private float _physicalDamageDealt;
        [SerializeField]
        private float _magicDamageDealt;
        [SerializeField]
        private float _trueDamageDealt;
        [Header("Actual Damage")]
        private float _actualPhysicalDamageDealt;
        [SerializeField]
        private float _actualMagicDamageDealt;
        [SerializeField]
        private float _actualTrueDamageDealt;

        [Header("Damage Taken Stats")]
        [Header("All Damage")]
        [SerializeField]
        private float _physicalDamageTaken;
        [SerializeField]
        private float _magicDamageTaken;
        [SerializeField]
        private float _trueDamageTaken;
        [Header("Blocked Damage")]
        [SerializeField]
        private float _physicalDamageBlocked;
        [SerializeField]
        private float _magicDamageBlocked;
        [SerializeField]
        private float _trueDamageBlocked;

        [Header("Health Healed Stats")]
        [SerializeField]
        private float _healthFromHealing;
        [SerializeField]
        private float _healthFromDamage;

        private GameObject _statBar;
        #endregion

        #region Properties
        public float PhysicalDamageDealt { get => _physicalDamageDealt; set => _physicalDamageDealt = value; }
        public float MagicDamageDealt { get => _magicDamageDealt; set => _magicDamageDealt = value; }
        public float TrueDamageDealt { get => _trueDamageDealt; set => _trueDamageDealt = value; }
        public GameObject StatBar { get => _statBar; set => _statBar = value; }
        public float PhysicalDamageTaken { get => _physicalDamageTaken; set => _physicalDamageTaken = value; }
        public float MagicDamageTaken { get => _magicDamageTaken; set => _magicDamageTaken = value; }
        public float TrueDamageTaken { get => _trueDamageTaken; set => _trueDamageTaken = value; }
        public float HealthFromHealing { get => _healthFromHealing; set => _healthFromHealing = value; }
        public float HealthFromDamage { get => _healthFromDamage; set => _healthFromDamage = value; }
        public float PhysicalDamageBlocked { get => _physicalDamageBlocked; set => _physicalDamageBlocked = value; }
        public float MagicDamageBlocked { get => _magicDamageBlocked; set => _magicDamageBlocked = value; }
        public float TrueDamageBlocked { get => _trueDamageBlocked; set => _trueDamageBlocked = value; }
        public float ActualPhysicalDamageDealt { get => _actualPhysicalDamageDealt; set => _actualPhysicalDamageDealt = value; }
        public float ActualMagicDamageDealt { get => _actualMagicDamageDealt; set => _actualMagicDamageDealt = value; }
        public float ActualTrueDamageDealt { get => _actualTrueDamageDealt; set => _actualTrueDamageDealt = value; }
        public float AllDamageDealt { get => _allDamageDealt; set => _allDamageDealt = value; }
        #endregion

        #region Methods

        public void ResetStats()
        {
            AllDamageDealt = 0;
            PhysicalDamageDealt = 0;
            MagicDamageDealt = 0;
            TrueDamageDealt = 0;

            ActualPhysicalDamageDealt = 0;
            ActualMagicDamageDealt = 0;
            ActualTrueDamageDealt = 0;

            PhysicalDamageTaken = 0;
            MagicDamageTaken = 0;
            TrueDamageTaken = 0;

            PhysicalDamageBlocked = 0;
            MagicDamageBlocked = 0;
            TrueDamageBlocked = 0;

            HealthFromHealing = 0;
            HealthFromDamage = 0;
        }

        public void AddDamageDealt(float phys, float mag, float tru)
        {
            PhysicalDamageDealt += phys;
            MagicDamageDealt += mag;
            TrueDamageDealt += tru;
            AllDamageDealt += phys + mag + tru;
        }

        public void AddActualDamageDealt(float phys, float mag, float tru)
        {
            ActualPhysicalDamageDealt += phys;
            ActualMagicDamageDealt += mag;
            ActualTrueDamageDealt += tru;
        }

        public void AddDamageTaken(float phys, float mag, float tru, float physB, float magB, float truB)
        {
            PhysicalDamageTaken += phys;
            MagicDamageTaken += mag;
            TrueDamageTaken += tru;

            PhysicalDamageBlocked += physB;
            MagicDamageBlocked += magB;
            TrueDamageBlocked += truB;
        }

        public void AddHealthHealed(float heal, float dmg)
        {
            HealthFromHealing += heal;
            HealthFromDamage += dmg;
        }
        #endregion
    }
}
