using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AutoBattles
{
    /// <summary>
    /// Takes care of displaying in game statistics
    /// </summary>
    public class StatManager : Singleton<StatManager>
    {
        #region Variables
        private Text _title;
        private Transform _listPanel;

        private float _ratioDmgDealt;
        private float _ratioDmgTaken;
        private float _ratioDmgBlocked;
        private float _ratioHealthHealed;

        private float _maxWidth;
        private bool _initialized;
        private int _mode; //1 = dmg dealt; 2 = dmg taken; 3 = dmg blocked; 4 = health healed;

        private List<IngameStats> _armyCopy;

        //references
        private UserInterfaceManager _UIManager;
        private ArmyManager _armyManagerScript;
        private GameManager _gameManagerScript;
        #endregion

        #region Properties
        //title of the displayed panel
        public Text Title { get => _title; set => _title = value; }
        public Transform ListPanel { get => _listPanel; set => _listPanel = value; }

        //ratio of shown bars for each panel
        public float RatioDmgDealt { get => _ratioDmgDealt; set => _ratioDmgDealt = value; }
        public float RatioDmgTaken { get => _ratioDmgTaken; set => _ratioDmgTaken = value; }
        public float RatioHealthHealed { get => _ratioHealthHealed; set => _ratioHealthHealed = value; }
        public float RatioDmgBlocked { get => _ratioDmgBlocked; set => _ratioDmgBlocked = value; }

        public float MaxWidth { get => _maxWidth; set => _maxWidth = value; }
        public bool Initialized { get => _initialized; set => _initialized = value; }
        //which statistic we're displaying
        public int Mode { get => _mode; set => _mode = value; }
        //list of all panels that we display in the panel
        public List<IngameStats> ArmyCopy { get => _armyCopy; set => _armyCopy = value; }

        //references
        public GameManager GameManagerScript { get => _gameManagerScript; set => _gameManagerScript = value; }
        public UserInterfaceManager UIManager { get => _UIManager; set => _UIManager = value; }
        public ArmyManager ArmyManagerScript { get => _armyManagerScript; set => _armyManagerScript = value; }
        #endregion

        #region Methods

        private void Awake()
        {
            ArmyCopy = new List<IngameStats>();
            GameManagerScript = GameManager.Instance;
            GameManagerScript.StatManagerScript = this;
            UIManager = UserInterfaceManager.Instance;
            ArmyManagerScript = ArmyManager.Instance;
            Title = transform.GetChild(0).GetComponent<Text>();
            ListPanel = transform.GetChild(1);
            Initialized = false;
            SwitchToDamageDealt();

            MaxWidth = ListPanel.GetChild(0).GetChild(2).GetComponent<Image>().rectTransform.sizeDelta.x;
        }

        //reset panels into default
        public void ResetValues()
        {
            //if nothing is displayed, just hide the panel
            if (ArmyManagerScript.ActivePlayerPawns.Count <= 0)
            {
                HidePanel();
                return;
            }

            //delete all pawn references
            if(ArmyCopy != null)
            {
                ArmyCopy.Clear();
            }
            Initialized = true;

            //set initial ratios
            RatioDmgDealt = MaxWidth / 5000;
            RatioDmgTaken = MaxWidth / 4000;
            RatioDmgBlocked = MaxWidth / 1000;
            RatioHealthHealed = MaxWidth / 1000;

            //reset statistics of all active pawns
            foreach (GameObject pawn in ArmyManagerScript.ActivePlayerPawns)
            {
                IngameStats statsScript = pawn.GetComponent<IngameStats>();
                statsScript.ResetStats();
            }

            //destroy all children of the panel except the template
            GameObject template = ListPanel.GetChild(0).gameObject;
            for(int i = 1; i < ListPanel.childCount; i++)
            {
                Destroy(ListPanel.GetChild(i).gameObject);
            }

            //create an empty bar for each active player pawn
            foreach (GameObject o in ArmyManagerScript.ActivePlayerPawns)
            {
                GameObject bar = Instantiate(template, ListPanel);
                bar.transform.GetChild(0).GetComponent<Image>().sprite = o.GetComponent<Pawn>().Stats.icon;
                SetBarFills(bar, 0, 0, 0, 0, 0, 0);
                bar.transform.GetComponentInChildren<Text>().text = "0";
                o.GetComponent<IngameStats>().StatBar = bar;
                ArmyCopy.Add(o.GetComponent<IngameStats>());
            }

            //get rid of the template
            Destroy(template);

            ShowPanel();
            //start coroutine that updates values in the bars
            StartCoroutine(StatTick());
        }

        //fill all bars to given values
        private void SetBarFills(GameObject bar, float fill1, float fill2, float fill3, float fill15, float fill25, float fill35)
        {
            //check if we work with all six bars
            if(fill15 != -1)
            {
                //set bar fills with appropriate color and offests
                Transform background = bar.transform.GetChild(2);
                Image barFill1 = background.GetChild(4).GetComponent<Image>();
                barFill1.rectTransform.sizeDelta = new Vector2(fill1, barFill1.rectTransform.rect.height);
                Image barFill2 = background.GetChild(2).GetComponent<Image>();
                barFill2.rectTransform.sizeDelta = new Vector2(fill15 + fill2, barFill2.rectTransform.rect.height);
                Image barFill3 = background.GetChild(0).GetComponent<Image>();
                barFill3.rectTransform.sizeDelta = new Vector2(fill15 + fill25 + fill3, barFill3.rectTransform.rect.height);

                Image barFill15 = background.GetChild(5).GetComponent<Image>();
                barFill15.rectTransform.sizeDelta = new Vector2(fill15, barFill15.rectTransform.rect.height);
                Image barFill25 = background.GetChild(3).GetComponent<Image>();
                barFill25.rectTransform.sizeDelta = new Vector2(fill15 + fill25, barFill25.rectTransform.rect.height);
                Image barFill35 = background.GetChild(1).GetComponent<Image>();
                barFill35.rectTransform.sizeDelta = new Vector2(fill15 + fill25 + fill35, barFill35.rectTransform.rect.height);
            }
            else
            {
                Transform background = bar.transform.GetChild(2);
                Image barFill1 = background.GetChild(4).GetComponent<Image>();
                barFill1.rectTransform.sizeDelta = new Vector2(fill1, barFill1.rectTransform.rect.height);
                Image barFill2 = background.GetChild(2).GetComponent<Image>();
                barFill2.rectTransform.sizeDelta = new Vector2(fill1 + fill2, barFill2.rectTransform.rect.height);
                Image barFill3 = background.GetChild(0).GetComponent<Image>();
                barFill3.rectTransform.sizeDelta = new Vector2(fill1 + fill2 + fill3, barFill3.rectTransform.rect.height);

                //if we work only with three bars, make the other three zero
                Image barFill15 = background.GetChild(5).GetComponent<Image>();
                barFill15.rectTransform.sizeDelta = new Vector2(0, barFill15.rectTransform.rect.height);
                Image barFill25 = background.GetChild(3).GetComponent<Image>();
                barFill25.rectTransform.sizeDelta = new Vector2(0, barFill25.rectTransform.rect.height);
                Image barFill35 = background.GetChild(1).GetComponent<Image>();
                barFill35.rectTransform.sizeDelta = new Vector2(0, barFill35.rectTransform.rect.height);
            }
            
        }

        //set colors of the bars
        private void SetBarColors(GameObject bar, Color color1, Color color2, Color color3)
        {
            Transform background = bar.transform.GetChild(2);
            Image barFill1 = background.GetChild(4).GetComponent<Image>();
            barFill1.color = color1;
            Image barFill2 = background.GetChild(2).GetComponent<Image>();
            barFill2.color = color2;
            Image barFill3 = background.GetChild(0).GetComponent<Image>();
            barFill3.color = color3;
        }

        //updates stats every 0.1 seconds
        IEnumerator StatTick()
        {
            yield return new WaitForSeconds(0.1f);

            RefreshStats();

            StartCoroutine("StatTick");
        }

        //stop updating values
        public void StopTick()
        {
            StopCoroutine(StatTick());
        }

        //refreshes stats
        private void RefreshStats()
        {
            //find out which values we are displaying
            switch (Mode)
            {
                case 1:
                    //update each pawn we are displaying
                    foreach (IngameStats statsScript in ArmyCopy)
                    {
                        if(statsScript == null)
                        {
                            continue;
                        }

                        //find out if we still fit into the bar, if not, change the ratio
                        float sum = statsScript.PhysicalDamageDealt + statsScript.MagicDamageDealt + statsScript.TrueDamageDealt;

                        if (sum * RatioDmgDealt > MaxWidth)
                        {
                            RatioDmgDealt *= 0.55f;
                        }

                        //set bar fills
                        //first is red, for actual physical damage dealt; second is white for mitigated physical damage dealt
                        //third is blue, for actual magic damage dealt; fourth is white for mitigated magic damage dealt
                        //fifth is pink, for actual true damage dealt; sixth is white for mitigated true damage dealt
                        SetBarFills(statsScript.StatBar, statsScript.ActualPhysicalDamageDealt * RatioDmgDealt, statsScript.ActualMagicDamageDealt * RatioDmgDealt,
                            statsScript.ActualTrueDamageDealt * RatioDmgDealt, statsScript.PhysicalDamageDealt * RatioDmgDealt, statsScript.MagicDamageDealt * RatioDmgDealt, 
                            statsScript.TrueDamageDealt * RatioDmgDealt);

                        //write the sum into text
                        statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
                    }
                    break;
                case 2:
                    foreach (IngameStats statsScript in ArmyCopy)
                    {
                        if (statsScript == null)
                        {
                            continue;
                        }

                        float sum = statsScript.PhysicalDamageTaken + statsScript.MagicDamageTaken + statsScript.TrueDamageTaken;

                        if (sum * RatioDmgTaken > MaxWidth)
                        {
                            RatioDmgTaken *= 0.55f;
                        }

                        //set bar fills
                        //first is red, for all physical damage taken
                        //third is blue, for all magic damage taken
                        //fifth is pink, for all true damage taken
                        SetBarFills(statsScript.StatBar, statsScript.PhysicalDamageTaken * RatioDmgTaken, statsScript.MagicDamageTaken * RatioDmgTaken, statsScript.TrueDamageTaken * RatioDmgTaken, -1, -1, -1);
                        
                        statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
                    }
                    break;
                case 3:
                    foreach (IngameStats statsScript in ArmyCopy)
                    {
                        if (statsScript == null)
                        {
                            continue;
                        }

                        float sum = statsScript.PhysicalDamageBlocked + statsScript.MagicDamageBlocked + statsScript.TrueDamageBlocked;

                        if (sum * RatioDmgBlocked > MaxWidth)
                        {
                            RatioDmgBlocked *= 0.55f;
                        }

                        //set bar fills
                        //first is red, for physical damage mitigated (blocked)
                        //third is blue, for magic damage mitigated (blocked)
                        //fifth is pink, for true damage mitigated (blocked)
                        SetBarFills(statsScript.StatBar, statsScript.PhysicalDamageBlocked * RatioDmgBlocked, statsScript.MagicDamageBlocked * RatioDmgBlocked, statsScript.TrueDamageBlocked * RatioDmgBlocked, -1, -1, -1);
                        
                        statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
                    }
                    break;
                case 4:
                    foreach (IngameStats statsScript in ArmyCopy)
                    {
                        if (statsScript == null)
                        {
                            continue;
                        }

                        float sum = statsScript.HealthFromHealing + statsScript.HealthFromDamage;

                        if (sum * RatioHealthHealed > MaxWidth)
                        {
                            RatioHealthHealed *= 0.55f;
                        }

                        //set bar fills
                        //first is green, for health healed from healing
                        //third is brown, for health healed from damage
                        SetBarFills(statsScript.StatBar, statsScript.HealthFromHealing * RatioHealthHealed, statsScript.HealthFromDamage * RatioHealthHealed, 0, -1, -1, -1);
                        
                        statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
                    }
                    break;
            }
            
        }

        public void HidePanel()
        {
            UIManager.IngameStatsPanel.Close();
        }

        //show panel only if the stats panel isn't open and this panel isn't empty
        public void ShowPanel()
        {
            if (!UIManager.StatsPanel.isActive && Initialized)
            {
                UIManager.IngameStatsPanel.Open();
            }
        }

        #region switch methods
        //these methods are called when player clicks button to change which stat is displayed
        //they work very similarly to the switch in RefreshStats(), the only difference is that they change title
        //of the panel and color of the bars because something else was displayed before

        public void SwitchToDamageDealt()
        {
            Title.text = "Damage Dealt";
            Mode = 1;
            foreach (IngameStats statsScript in ArmyCopy)
            {
                if (statsScript == null)
                {
                    continue;
                }
                
                SetBarColors(statsScript.StatBar, new Color(1, 0, 0), new Color(0, 0, 1), new Color(1, 0.18f, 0.78f));

                float sum = statsScript.PhysicalDamageDealt + statsScript.MagicDamageDealt + statsScript.TrueDamageDealt;
                if (sum * RatioDmgDealt > MaxWidth)
                {
                    RatioDmgDealt *= 0.55f;
                }

                SetBarFills(statsScript.StatBar, statsScript.ActualPhysicalDamageDealt * RatioDmgDealt, statsScript.ActualMagicDamageDealt * RatioDmgDealt,
                            statsScript.ActualTrueDamageDealt * RatioDmgDealt, statsScript.PhysicalDamageDealt * RatioDmgDealt, statsScript.MagicDamageDealt * RatioDmgDealt,
                            statsScript.TrueDamageDealt * RatioDmgDealt);

                statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
            }
        }

        public void SwitchToDamageTaken()
        {
            Title.text = "Damage Taken";
            Mode = 2;
            foreach (IngameStats statsScript in ArmyCopy)
            {
                if (statsScript == null)
                {
                    continue;
                }

                SetBarColors(statsScript.StatBar, new Color(1, 0, 0), new Color(0, 0, 1), new Color(1, 0.18f, 0.78f));

                float sum = statsScript.PhysicalDamageTaken + statsScript.MagicDamageTaken + statsScript.TrueDamageTaken;
                if (sum * RatioDmgTaken > MaxWidth)
                {
                    RatioDmgTaken *= 0.55f;
                }
                
                SetBarFills(statsScript.StatBar, statsScript.PhysicalDamageTaken * RatioDmgTaken, statsScript.MagicDamageTaken * RatioDmgTaken, statsScript.TrueDamageTaken * RatioDmgTaken, -1, -1, -1);
                
                statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
            }
        }

        public void SwitchToDamageBlocked()
        {
            Title.text = "Damage Blocked";
            Mode = 3;
            foreach (IngameStats statsScript in ArmyCopy)
            {
                if (statsScript == null)
                {
                    continue;
                }

                SetBarColors(statsScript.StatBar, new Color(1, 0, 0), new Color(0, 0, 1), new Color(1, 0.18f, 0.78f));
                
                float sum = statsScript.PhysicalDamageBlocked + statsScript.MagicDamageBlocked + statsScript.TrueDamageBlocked;
                if (sum * RatioDmgBlocked > MaxWidth)
                {
                    RatioDmgBlocked *= 0.55f;
                }

                SetBarFills(statsScript.StatBar, statsScript.PhysicalDamageBlocked * RatioDmgBlocked, statsScript.MagicDamageBlocked * RatioDmgBlocked, statsScript.TrueDamageBlocked * RatioDmgBlocked, -1, -1, -1);
                
                statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
            }
        }

        public void SwitchToHealthHealed()
        {
            Title.text = "Health Healed";
            Mode = 4;
            foreach (IngameStats statsScript in ArmyCopy)
            {
                if (statsScript == null)
                {
                    continue;
                }

                SetBarColors(statsScript.StatBar, new Color(0, 1, 0), new Color(0.38f, 0.18f, 0.11f), new Color(0, 0, 0));
               
                float sum = statsScript.HealthFromHealing + statsScript.HealthFromDamage;
                if (sum * RatioHealthHealed > MaxWidth)
                {
                    RatioHealthHealed *= 0.55f;
                }
                
                SetBarFills(statsScript.StatBar, statsScript.HealthFromHealing * RatioHealthHealed, statsScript.HealthFromDamage * RatioHealthHealed, 0, -1, -1, -1);
                
                statsScript.StatBar.GetComponentInChildren<Text>().text = sum.ToString();
            }
        }
        #endregion

        //if a pawn is sold, destroy its stat bar
        public void PawnSold(GameObject pawn)
        {
            GameObject s = pawn.GetComponent<IngameStats>().StatBar;
            if (s != null)
            {
                //if it's last bar in the panel, keep it as template and hide the panel
                //since it's technically empty now
                if(ListPanel.childCount <= 1)
                {
                    HidePanel();
                    Initialized = false;
                }
                else
                {
                    s.transform.SetParent(null);
                    Destroy(s);
                }
            }
        }
        #endregion
    }
}
