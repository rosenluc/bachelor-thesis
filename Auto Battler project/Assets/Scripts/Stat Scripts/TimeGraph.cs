using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AutoBattles
{
    /// <summary>
    /// This script creates a graph that shows dependence of a value on time (represented by round number)
    /// </summary>
    public class TimeGraph : MonoBehaviour
    {
        #region Variables
        [SerializeField]
        private Sprite _pointSprite;

        private GameObject _labelX;
        private GameObject _labelY;

        private RectTransform _graph;
        //true if the graph is already created
        private bool initialized;
        //changes depending on number of lines, that the graph displays
        private int index;
        #endregion

        #region Properties
        public Sprite PointSprite { get => _pointSprite; set => _pointSprite = value; }
        public GameObject LabelX { get => _labelX; set => _labelX = value; }
        public GameObject LabelY { get => _labelY; set => _labelY = value; }
        public RectTransform Graph { get => _graph; set => _graph = value; }
        #endregion

        #region Methods

        private void Awake()
        {
            Graph = GetComponent<RectTransform>();
            //get label templates so we can later copy them into scale we need
            LabelX = transform.GetChild(2).gameObject;
            LabelY = transform.GetChild(1).gameObject;

            initialized = false;
            //hide the graph
            gameObject.SetActive(false);
        }

        //creates a point in the given position
        private GameObject CreatePoint(Vector2 anchoredPosition, string value)
        {
            //create the point
            GameObject point = new GameObject("point " + value, typeof(Image));
            point.transform.SetParent(Graph);
            point.GetComponent<Image>().sprite = PointSprite;

            //set its position
            RectTransform rTrans = point.GetComponent<RectTransform>();
            rTrans.anchoredPosition = anchoredPosition;
            rTrans.sizeDelta = new Vector2(11, 11);
            rTrans.anchorMin = new Vector2(0, 0);
            rTrans.anchorMax = new Vector2(0, 0);
            return point;
        }

        //creates the graph from one or two lists of values
        private void CreateGraph(List<float> values, List<float> values2)
        {
            index = 3;
            initialized = true;
            Color color = new Color(1, 1, 1, 0.5f);

            //initial max value for y axis is 20
            float maxValueY = 20;

            //if we find greater value than 20, we have to scale y more
            foreach(float v in values)
            {
                if (v > maxValueY)
                {
                    maxValueY = v;
                }
            }

            //if the second list isn't null, we will be creating two lines in this graph
            if (values2 != null)
            {
                //that means we also need to investigate the other list for greatest value
                foreach (float v in values2)
                {
                    if (v > maxValueY)
                    {
                        maxValueY = v;
                    }
                }
                //we set color and index that indicate we have two lines not one
                color = new Color(1, 0, 0, 0.5f);
                index = 4;
            }

            //similarly to y, we look for the greatest value on x axis, we set default to 10
            float maxValueX = 10;

            //x value is indicated by length of the list
            if(values.Count > 10)
            {
                maxValueX = values.Count;
            }

            if (values2 != null)
            {
                if (values2.Count > 10)
                {
                    maxValueX = values2.Count;
                }
            }

            float graphHeight = Graph.sizeDelta.y;
            float graphWidth = Graph.sizeDelta.x;

            GameObject lastPoint = null;
            //for each value, calculate its point position and create the point
            for (int i = 0; i < values.Count; i++)
            {
                float xPos = (i / maxValueX) * graphWidth;
                
                float yPos = (values[i] / maxValueY) * graphHeight;
                GameObject newPoint = CreatePoint(new Vector2(xPos, yPos), values[i].ToString());
                //if the point isn't the first point, connect it with the previous one
                if (lastPoint)
                {
                    ConnectDots(lastPoint.GetComponent<RectTransform>().anchoredPosition, newPoint.GetComponent<RectTransform>().anchoredPosition, color);
                }
                lastPoint = newPoint;
            }

            //draw the second line if we have two line graph
            if (values2 != null)
            {
                lastPoint = null;
                for (int i = 0; i < values2.Count; i++)
                {
                    float xPos = (i / maxValueX) * graphWidth;

                    float yPos = (values2[i] / maxValueY) * graphHeight;
                    GameObject newPoint = CreatePoint(new Vector2(xPos, yPos), values2[i].ToString());
                    if (lastPoint)
                    {
                        ConnectDots(lastPoint.GetComponent<RectTransform>().anchoredPosition, newPoint.GetComponent<RectTransform>().anchoredPosition, new Color(0, 0, 1, 0.5f));
                    }
                    lastPoint = newPoint;
                }
            }

            //create labels accroding to graph sizes and values
            CreateLabels(graphHeight, Graph.sizeDelta.x, maxValueY, maxValueX);
        }

        //creates labels according to graph scale
        private void CreateLabels(float graphHeight, float graphWidth, float maxValueY, float maxValueX)
        {
            //we want to always show 10 numbers on x axis
            //calculate how big skips we need to do between values to fit the greatest x value we need
            float ratioX = graphWidth / 10;
            float a = maxValueX / 10;
            for (int i = 1; i < 10; i++)
            {
                GameObject label = Instantiate(LabelX, Graph);
                Vector3 pos = LabelX.transform.localPosition;
                label.transform.localPosition = new Vector3(pos.x + ratioX * i, pos.y, pos.z);
                label.GetComponent<Text>().text = ((int)Math.Round(a * i)).ToString();
            }

            //we want to always show 5 numbers on y axis
            //calculate how big skips we need to do between values to fit the greatest y value we need
            float ratioY = graphHeight / 5;
            float b = maxValueY / 5;
            for (int i = 1; i <= 5; i++)
            {
                GameObject label = Instantiate(LabelY, Graph);
                Vector3 pos = LabelY.transform.localPosition;
                label.transform.localPosition = new Vector3(pos.x, pos.y + ratioY * i, pos.z);
                label.GetComponent<Text>().text = ((int)Math.Round(b * i)).ToString();
            }
        }

        //connects two points with a line
        private void ConnectDots(Vector2 pointA, Vector2 pointB, Color color)
        {
            //create the line
            GameObject line = new GameObject("line", typeof(Image));
            line.transform.SetParent(Graph);
            line.GetComponent<Image>().color = color;

            //calculate its rotation and scale
            Vector2 direction = (pointB - pointA).normalized;
            float distance = Vector2.Distance(pointA, pointB);

            //set the line
            RectTransform rTrans = line.GetComponent<RectTransform>();
            rTrans.localScale = new Vector3(1, 1, 1);
            rTrans.sizeDelta = new Vector2(distance, 3f);
            rTrans.anchorMin = new Vector2(0, 0);
            rTrans.anchorMax = new Vector2(0, 0);
            rTrans.localEulerAngles = new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI);
            rTrans.anchoredPosition = pointA + direction * distance * 0.5f;
        }

        //destroy the graph except template
        public void DestroyGraph()
        {
            //the graph hasn't been initialized yet
            if(index == 0)
            {
                initialized = false;
                HideGraph();
                return;
            }

            //destroy all children except template (template is background and two labels + legend in case of two line graph)
            for(int i = index; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            initialized = false;
            HideGraph();
        }

        public void HideGraph()
        {
            gameObject.SetActive(false);
        }

        //show graph and create it, if it hasn't been initialized yet
        public void ShowGraph(List<float> values, List<float> values2)
        {
            if (!initialized)
            {
                CreateGraph(values, values2);
            }
            gameObject.SetActive(true);
        }
        #endregion
    }
}
