﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for enemy decision making
/// </summary>

namespace AutoBattles
{
    public class EnemyRosterManager : Singleton<EnemyRosterManager>
    {
        #region Variables
        private int _gold;
        private int _experience;
        private int _level;
        private int _maxLevel;
        private int _maxExperience;
        private List<GameObject> _bench;
        private int _maxBenchSlots;
        private List<GameObject> _army;

        //references
        private ArmyManager _armyManagerScript;
        private GameManager _gameManagerScript;
        private ChessBoardManager _boardManager;
        #endregion

        #region Properties
        public int Gold { get => _gold; set => _gold = value; }
        public int Experience { get => _experience; set => _experience = value; }
        public int Level { get => _level; set => _level = value; }
        public int MaxLevel { get => _maxLevel; set => _maxLevel = value; }
        public int MaxExperience { get => _maxExperience; set => _maxExperience = value; }
        public int MaxBenchSlots { get => _maxBenchSlots; set => _maxBenchSlots = value; }
        //list of pawns on enemy bench
        public List<GameObject> Bench { get => _bench; set => _bench = value; }
        //enemy army
        public List<GameObject> Army { get => _army; set => _army = value; }

        //references
        public ArmyManager ArmyManagerScript { get => _armyManagerScript; set => _armyManagerScript = value; }
        public GameManager GameManagerScript { get => _gameManagerScript; set => _gameManagerScript = value; }
        public ChessBoardManager BoardManager { get => _boardManager; set => _boardManager = value; }
        #endregion

        #region Methods

        protected virtual void Awake()
        {
            //set the starting values
            Gold = 2;
            Experience = 0;
            Level = 0;
            MaxLevel = 6;
            MaxExperience = 1;
            MaxBenchSlots = 8;
            Bench = new List<GameObject>();
            Army = new List<GameObject>();

            ArmyManagerScript = ArmyManager.Instance;
            if (!ArmyManagerScript)
            {
                Debug.LogError("No ArmyManager singleton instance found in the scene. Please add an ArmyManager script to the GameManager gamobject before entering playmode.");
            }

            GameManagerScript = GameManager.Instance;
            if (!GameManagerScript)
            {
                Debug.LogError("No GameManager singleton instance found in the scene. Please add an ArmyManager script to the GameManager gamobject before entering playmode.");
            }

            BoardManager = ChessBoardManager.Instance;
            if (!BoardManager)
            {
                Debug.LogError("No ChessBoardManager singleton instance found in the scene. Please add a ChessBoardManager script to the Game Mananger gameobject before" +
                    "entering playmode!");
            }
        }

        //sets starting values and deletes all pawns
        public void ResetRoster()
        {
            Gold = 2;
            Experience = 0;
            Level = 0;
            MaxLevel = 6;
            MaxExperience = 1;
            MaxBenchSlots = 8;
            ArmyManagerScript.EnemyMaxArmySize = 0;

            foreach (GameObject o in Bench)
            {
                o.GetComponent<Status>().SelfDestructNoTile();
            }
            Bench.Clear();

            foreach(GameObject o in Army)
            {
                o.GetComponent<Status>().SelfDestruct();
            }
            Army.Clear();
        }

        //returns army and bench in string
        private string ArmyInString()
        {
            string result = "Active: " + Army.Count + " ";
            foreach(GameObject o in Army)
            {
                result += o.GetComponent<Pawn>().Stats.name;
                result += " ";
            }
            result += "\nBench: " + Bench.Count + " ";
            foreach (GameObject o in Bench)
            {
                result += o.GetComponent<Pawn>().Stats.name;
                result += " ";
            }
            return result;
        }

        //determines which available position is the best for the pawn
        private ChessBoardTile DeterminePosition(PawnStats pawn)
        {
            int i = 0;
            int j = 0;
            //set the middle in best row for the pawn according to its range
            switch (pawn.AARange)
            {
                case 1:
                    j = 35;
                    break;
                case 2:
                    j = 43;
                    break;
                case 3:
                    j = 51;
                    break;
                case 4:
                    j = 59;
                    break;
            }

            //find the closest available tile to the ideal one
            while (true)
            {
                if(j + i < 64)
                {
                    if (BoardManager.ChessBoardTiles[j + i].ActivePawn == null)
                    {
                        return BoardManager.ChessBoardTiles[j + i];
                    }
                }
                if(j - i > 31)
                {
                    if (BoardManager.ChessBoardTiles[j - i].ActivePawn == null)
                    {
                        return BoardManager.ChessBoardTiles[j - i];
                    }
                }
                i++;
            }
        }

        //decide the best move for enemy and perform it 
        public void DecideMove()
        {
            //prints the current enemy state
            Debug.Log($"Enemy state: {Gold} g, {Level} lvl, {Experience} xp");
            Debug.Log(ArmyInString());

            //get pawns in shop
            PawnStats[] pawnsInShop = GameManagerScript.RerollShop();

            START:
            //we want to have as many active pawns as we can
            while (ArmyManagerScript.EnemyMaxArmySize > Army.Count)
            {
                //if the shop is empty and we have enough gold, reroll
                if(pawnsInShop.Length == 0 && Gold >= 2)
                {
                    Gold -= 2;
                    pawnsInShop = GameManagerScript.RerollShop();
                }

                Debug.Log("Army too small");
                //find out the best purchase we can make
                PawnStats newPawn = BestNewPurchase(pawnsInShop);
                
                //if there is such purchase, perform it
                if(newPawn != null)
                {
                    //remove bought instance from shop
                    List<PawnStats> tmp = new List<PawnStats>(pawnsInShop);
                    tmp.RemoveAt(tmp.IndexOf(newPawn));
                    pawnsInShop = tmp.ToArray();

                    Debug.Log("Purchase " + newPawn.name);
                    //create the purchased pawn
                    int cost = CalculateCost(newPawn.pawnQuality, PawnStats.StarRating.One);
                    Gold -= cost;
                    GameObject p = Instantiate(newPawn.pawn);
                    Status status = p.GetComponent<Status>();
                    status.IsPlayer = false;
                    status.GoldWorth = cost;
                    //add the new pawn to the enemy army
                    Army.Add(p);
                    ChessBoardTile tile = DeterminePosition(newPawn);
                    tile.ChangePawnOutOfCombat(p);
                    CheckForCombination(p.GetComponent<Pawn>().Stats);
                }
                //if there is no purchase to make, move pawn from bench to board
                else if(Bench.Count != 0)
                {
                    //get the first pawn from bench and add it to army
                    GameObject pawnFromBench = Bench[0];
                    Debug.Log("Added from bench " + pawnFromBench.name);
                    Army.Add(pawnFromBench);

                    pawnFromBench.SetActive(true);

                    //remove pawn from the bench
                    Bench.RemoveAt(0);
                    ChessBoardTile tile = DeterminePosition(pawnFromBench.GetComponent<Pawn>().Stats);
                    tile.ChangePawnOutOfCombat(pawnFromBench);
                }
                //if we cannot buy anything and we have nothing on bench, end the loop
                //there is nothing we can do to fill the army to its full capacity
                else
                {
                    break;
                }
            }

            //if we have some gold left, we might want to buy something more
            if(Gold > 0)
            {
                //search if there is any good purchase we can make for later pawn combinations
                PawnStats newPawn = BestCombinationPurchase(pawnsInShop);

                //if there is such purchase, perform it
                if(newPawn != null)
                {
                    Debug.Log("Buy " + newPawn.name + " to combine.");
                    //create the pawn
                    int cost = CalculateCost(newPawn.pawnQuality, PawnStats.StarRating.One);
                    Gold -= cost;
                    GameObject p = Instantiate(newPawn.pawn);
                    Status status = p.GetComponent<Status>();
                    status.IsPlayer = false;
                    status.GoldWorth = cost;
                    //add the pawn to bench
                    Bench.Add(p);
                    p.SetActive(false);
                    CheckForCombination(p.GetComponent<Pawn>().Stats);
                }
                //if we have enough gold and we aren't max level, buy experience
                else if(Gold >= 5 && Level < 6)
                {
                    Debug.Log("Buy experience");
                    //if we have more than 50 gold, we want to continue buying experience
                    //since the maximum interest is from 50 gold anyway
                    do
                    {
                        GainExperience(4);
                        Gold -= 5;
                    } while (Gold > 50 && Level < 6);
                    //if we purchased experience, we might have leveled up
                    //in that case, we want to add pawns to board
                    goto START;
                }
            }

            //update the army in Army Script
            ArmyManagerScript.ChangeActiveEnemyRoster(Army);
        }

        //upgrades three same pawns into one stronger
        private void UpgradePawn(PawnStats newPawn)
        {
            GameObject item = null;
            int i = 0;
            //go through whole army and find the three pawns we will be upgrading
            for (int j = 0; j < Army.Count; j++)
            {
                GameObject o = Army[j];
                //if we have found all three, end the search
                if (i == 3)
                {
                    break;
                }
                //if the pawn is the same as new pawn, delete it
                if (o.GetComponent<Pawn>().Stats.name == newPawn.name && o.GetComponent<Pawn>().Stats.pawnQuality == newPawn.pawnQuality)
                {
                    i++;
                    Army.Remove(o);
                    //if the pawn has an item, note it
                    //we want to place that item on someone else
                    if (o.GetComponent<Pawn>().HasItem)
                    {
                        item = o.GetComponent<Pawn>().EquippedItem.gameObject;
                    }
                    o.GetComponent<Status>().SelfDestruct();
                }
            }

            //if we didn't find these pawns in the army, search the bench too
            for (int j = 0; j < Bench.Count; j++)
            {
                GameObject o = Bench[j];
                if (i == 3)
                {
                    break;
                }
                if (o.GetComponent<Pawn>().Stats.name == newPawn.name && o.GetComponent<Pawn>().Stats.pawnQuality == newPawn.pawnQuality)
                {
                    i++;
                    Bench.Remove(o);
                    if (o.GetComponent<Pawn>().HasItem)
                    {
                        item = o.GetComponent<Pawn>().EquippedItem.gameObject;
                    }
                    o.GetComponent<Status>().SelfDestructNoTile();
                }
            }

            //create the new upgraded pawn
            GameObject p = Instantiate(newPawn.upgradedPawn.pawn);
            Status status = p.GetComponent<Status>();
            status.IsPlayer = false;
            status.GoldWorth = CalculateCost(newPawn.pawnQuality, p.GetComponent<Pawn>().Stats.starRating);
            //add the upgraded pawn to army
            Army.Add(p);
            ChessBoardTile tile = DeterminePosition(newPawn);
            tile.ChangePawnOutOfCombat(p);
            //if some of the pawns had item, place it
            if(item != null)
            {
                PlaceOldItem(item);
            }
        }

        //finds out if the new pawn we purchased was enough to upgrade
        private void CheckForCombination(PawnStats newPawn)
        {
            //if we have three same one star pawns, upgrade 
            if(NumberOfStarPawnsOwned(newPawn, 1) == 3)
            {
                Debug.Log("Upgrading " + newPawn.name + " into 2-star.");
                UpgradePawn(newPawn);
                //after the upgrade, we have a new 2 star pawn
                //check if we can upgrade it to 3 star
                if(NumberOfStarPawnsOwned(newPawn, 2) == 3)
                {
                    Debug.Log("Upgrading " + newPawn.name + " into 3-star.");
                    UpgradePawn(newPawn.upgradedPawn);
                }
            }
        }

        //counts how many pawns from certain class with certain number of stars we have
        private int NumberOfStarPawnsOwned(PawnStats pawn, int stars)
        {
            int owned = 0;
            //searches bench
            foreach (GameObject o in Bench)
            {
                PawnStats s = o.GetComponent<Pawn>().Stats;
                if (s.name == pawn.name)
                {
                    switch (stars)
                    {
                        case 1:
                            if (s.starRating == PawnStats.StarRating.One)
                            {
                                owned++;
                            }
                            break;
                        case 2:
                            if (s.starRating == PawnStats.StarRating.Two)
                            {
                                owned++;
                            }
                            break;
                    }
                }
            }
            //searches army
            foreach (GameObject o in Army)
            {
                PawnStats s = o.GetComponent<Pawn>().Stats;
                if (s.name == pawn.name)
                {
                    switch (stars)
                    {
                        case 1:
                            if (s.starRating == PawnStats.StarRating.One)
                            {
                                owned++;
                            }
                            break;
                        case 2:
                            if (s.starRating == PawnStats.StarRating.Two)
                            {
                                owned++;
                            }
                            break;
                    }
                }
            }
            //returns number of pawns it found
            return owned;
        }

        //chooses suitable pawn for item we want to place
        private GameObject ChoosePawnForItem(GameObject item)
        {
            //if the item is an emblem, we need to choose a pawn
            //that doesn't have the trait the emblem provides
            if (item.name == "Human Emblem")
            {
                foreach(GameObject o in Army)
                {
                    if (!o.GetComponent<Pawn>().Stats.origins.Contains(PawnStats.Origin.Human))
                    {
                        return o;
                    }
                }
                foreach (GameObject o in Bench)
                {
                    if (!o.GetComponent<Pawn>().Stats.origins.Contains(PawnStats.Origin.Human))
                    {
                        return o;
                    }
                }
                return null;
            }
            else if(item.name == "Orc Emblem")
            {
                foreach (GameObject o in Army)
                {
                    if (!o.GetComponent<Pawn>().Stats.origins.Contains(PawnStats.Origin.Orc))
                    {
                        return o;
                    }
                }
                foreach (GameObject o in Bench)
                {
                    if (!o.GetComponent<Pawn>().Stats.origins.Contains(PawnStats.Origin.Orc))
                    {
                        return o;
                    }
                }
                return null;
            }

            //otherwise we just through the pawns we have and choose the first one without item
            foreach (GameObject o in Army)
            {
                if (!o.GetComponent<Pawn>().HasItem)
                {
                    return o;
                }
            }
            
            foreach (GameObject o in Bench)
            {
                if (!o.GetComponent<Pawn>().HasItem)
                {
                    return o;
                }
            }
            return null;
        }

        //places item we got on a pawn
        public void PlaceItem(ItemStats itemStat)
        {
            //create the item
            GameObject item = Instantiate(itemStat.item);
            item.name = itemStat.name;
            item.GetComponent<Item>().Setup(itemStat);
            item.GetComponentInChildren<Image>().sprite = itemStat.icon;

            //choose a pawn we will place it on
            GameObject pawn = ChoosePawnForItem(item);
            //if there is no suitable pawn, just forget it
            if(pawn == null)
            {
                return;
            }

            Debug.Log("Placing " + itemStat.name + " on " + pawn.name);

            //equip the item
            if (itemStat.name == "Orc Emblem" || itemStat.name == "Human Emblem")
            {
                Debug.Log("Emblem should never be option for NPC.");
            }
            else
            {
                pawn.GetComponent<Pawn>().Equip(item.GetComponent<Item>());
            }
            //set the item to the healthbar of the pawn
            item.transform.SetParent(pawn.GetComponent<HealthAndMana>().HealthBarTransform);
            item.GetComponent<RectTransform>().localPosition = new Vector3(18.8f, 9.5f, 0);
            item.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            item.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        //place already existing item onto a new pawn
        //called after selling or upgrading a pawn with item
        public void PlaceOldItem(GameObject item)
        {
            //find suitable pawn
            GameObject pawn = ChoosePawnForItem(item);
            if (pawn == null)
            {
                return;
            }

            Debug.Log("Placing " + item.name + " from sold pawn on " + pawn.name);

            //equip the pawn
            if (item.name == "Orc Emblem" || item.name == "Human Emblem")
            {
                pawn.GetComponent<Pawn>().Equip(item.GetComponent<Item>());
            }
            else
            {
                pawn.GetComponent<Pawn>().Equip(item.GetComponent<Item>());
            }
            //set the item to the pawn's healthbar
            item.transform.SetParent(pawn.GetComponent<HealthAndMana>().HealthBarTransform);
            item.GetComponent<RectTransform>().localPosition = new Vector3(18.8f, 9.5f, 0);
            item.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        //finds the best purchase we can make for future combining
        private PawnStats BestCombinationPurchase(PawnStats[] shop)
        {
            PawnStats best = null;
            int n = 0;
            //goes through all pawns in shop
            foreach (PawnStats p in shop)
            {
                int cost = CalculateCost(p.pawnQuality, PawnStats.StarRating.One);
                //checks if we have enough gold to buy the pawn
                if (cost < Gold)
                {
                    n = NumberOfPawnsOwned(p)%9;
                    //we buy the pawn that we have most instances of already
                    //if we have more than nine however, we cannot combine it into anything more than 3 star
                    //so it counts as separate pawn
                    if (best == null)
                    {
                        if (n > 0)
                        {
                            best = p;
                        }
                    } else if (n > NumberOfPawnsOwned(best))
                    {
                        best = p;
                    }
                }
            }

            //if we found something but we have nowhere to place it
            //find out if we have something less valuable we can sell
            if(Bench.Count >= MaxBenchSlots && best != null)
            {
                GameObject remove = null;
                int min = n;
                foreach (GameObject o in Bench)
                {
                    //if we have less instances of some pawn we already own
                    //than of our best new buy, we note it
                    int on = NumberOfPawnsOwned(o.GetComponent<Pawn>().Stats)%9;
                    if(on < n || on < min)
                    {
                        min = on;
                        remove = o;
                    }
                }
                //if we didn't find anything good to sell, we have no good buy
                if (remove == null)
                {
                    return null;
                }
                //otherwise we sell the least valueable pawn
                else
                {
                    Debug.Log("Sold " + remove.GetComponent<Pawn>().Stats.name + " from the bench to make space.");
                    //if it had item, place it on someone else
                    if (remove.GetComponent<Pawn>().HasItem)
                    {
                        PlaceOldItem(remove.GetComponent<Pawn>().EquippedItem.gameObject);
                    }
                    //get gold for the sell and remove the pawn
                    GainGold(remove.GetComponent<Status>().GoldWorth);
                    Bench.Remove(remove);
                    remove.GetComponent<Status>().SelfDestruct();
                }
            }
            
            return best;
        }

        //count all instances of the pawn we have
        private int NumberOfPawnsOwned(PawnStats pawn)
        {
            int owned = 0;
            //search the bench
            foreach (GameObject o in Bench)
            {
                PawnStats s = o.GetComponent<Pawn>().Stats;
                if (s.name == pawn.name)
                {
                    switch (s.starRating)
                    {
                        case PawnStats.StarRating.One:
                            owned++;
                            break;
                        case PawnStats.StarRating.Two:
                            //two star pawns are made of three one star pawns
                            owned += 3;
                            break;
                        case PawnStats.StarRating.Three:
                            //three star pawn is made of 3 two star pawns
                            owned += 9;
                            break;
                    }
                }
            }
            //search the army
            foreach (GameObject o in Army)
            {
                PawnStats s = o.GetComponent<Pawn>().Stats;
                if (s.name == pawn.name)
                {
                    switch (s.starRating)
                    {
                        case PawnStats.StarRating.One:
                            owned++;
                            break;
                        case PawnStats.StarRating.Two:
                            owned += 3;
                            break;
                        case PawnStats.StarRating.Three:
                            owned += 9;
                            break;
                    }
                }
            }
            return owned;
        }

        //find the best new purchase we can make
        private PawnStats BestNewPurchase(PawnStats[] shop)
        {
            //if the shop is empty, return
            if(shop.Length == 0)
            {
                return null;
            }
            PawnStats best = null;
            PawnStats.StarRating star = PawnStats.StarRating.One;
            //go through the shop
            foreach(PawnStats p in shop)
            {
                int cost = CalculateCost(p.pawnQuality, star);
                //if we have enough gold for the pawn, examine it
                if (cost < Gold)
                {
                    //if we didn't choose anything yet, this is our best choice
                    if(best == null)
                    {
                        best = p;
                        continue;
                    }
                    else if (!PawnOwned(p))
                    {
                        //we want the pawn we don't have yet
                        //if we don't have either, we want the more expensive one
                        if (!PawnOwned(best))
                        {
                            if (cost > CalculateCost(best.pawnQuality, star))
                            {
                                best = p;
                            }
                        }
                        else
                        {
                            best = p;
                        }
                    }
                    else
                    {
                        //if we have both pawns, we want the more expensive one
                        if (PawnOwned(best))
                        {
                            if (cost > CalculateCost(best.pawnQuality, star)){
                                best = p;
                            }
                        }
                    }
                }
            }
            return best;
        }

        //returns cost of the pawn
        private int CalculateCost(PawnStats.PawnQuality quality, PawnStats.StarRating stars)
        {
            int cost = 1;

            switch (quality)
            {
                case PawnStats.PawnQuality.Common:
                    {
                        switch (stars)
                        {
                            case PawnStats.StarRating.One:
                                break;
                            case PawnStats.StarRating.Two:
                                cost = 3;
                                break;
                            case PawnStats.StarRating.Three:
                                cost = 8;
                                break;
                        }
                        break;
                    }
                case PawnStats.PawnQuality.Uncommon:
                    {
                        switch (stars)
                        {
                            case PawnStats.StarRating.One:
                                cost = 2;
                                break;
                            case PawnStats.StarRating.Two:
                                cost = 5;
                                break;
                            case PawnStats.StarRating.Three:
                                cost = 13;
                                break;
                        }
                        break;
                    }
                case PawnStats.PawnQuality.Rare:
                    {
                        switch (stars)
                        {
                            case PawnStats.StarRating.One:
                                cost = 3;
                                break;
                            case PawnStats.StarRating.Two:
                                cost = 7;
                                break;
                            case PawnStats.StarRating.Three:
                                cost = 20;
                                break;
                        }
                        break;
                    }
            }
            return cost;
        }

        //true if we already have that pawn in army
        private bool PawnOwned(PawnStats pawn)
        {
            foreach(GameObject o in Army)
            {
                if(o.GetComponent<Pawn>().Stats.name == pawn.name)
                {
                    return true;
                }
            }
            return false;
        }

        public void GainGold(int g)
        {
            Gold += g;
        }

        public void GainExperience(int e)
        {
            if (Level == MaxLevel)
                return;

            Experience += e;

            if (Experience >= MaxExperience)
            {
                LevelUp();
            }
        } 

        //adds level and increases army size
        protected virtual void LevelUp()
        {
            Level += 1;

            Experience -= MaxExperience;

            IncreaseMaxExperience();

            ArmyManagerScript.IncreaseEnemyMaxArmySize(1);
        }

        //increses the experience we need to reach next level
        protected virtual void IncreaseMaxExperience()
        {
            switch (Level)
            {
                case 1:
                    MaxExperience = 2;
                    break;
                case 2:
                    MaxExperience = 8;
                    break;
                case 3:
                    MaxExperience = 16;
                    break;
                case 4:
                    MaxExperience = 24;
                    break;
                case 5:
                    MaxExperience = 40;
                    break;
            }
        }

        public virtual void ResetExperience()
        {
            Level = 0;
            Experience = 0;
            MaxExperience = 1;
        }
        #endregion
    }
}

