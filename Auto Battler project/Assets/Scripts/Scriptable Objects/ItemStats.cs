using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is a data container for the base item stats
/// </summary>

namespace AutoBattles
{
    [CreateAssetMenu(fileName = "New Item Stats", menuName = "Custom/ItemStats")]
    public class ItemStats : ScriptableObject
    {
        [Header("GENERAL INFO")]
        public new string name;
        public Sprite icon;
        public GameObject item;
        public ItemQuality itemQuality;

        [Header("ATK")]
        [Tooltip("Physical damage a this item provides.")]
        public float PhysicalDamage;
        [Tooltip("Magic damage a this item provides.")]
        public float MagicDamage;
        [Tooltip("True damage a this item provides.")]
        public float TrueDamage;

        [Header("OTHER STATS")]
        [Tooltip("The amount of mana regeneration this item provides.")]
        public float ManaRegen;
        [Tooltip("Description of what the item does.")]
        [TextArea(3, 5)]
        public string Description;

        [Header("DEF")]
        [Tooltip("The amount of health this item provides.")]
        public float Health;
        [Tooltip("The amount of armor this item provides.")]
        public float Armor;
        [Tooltip("The amount of resistance this item provides.")]
        public float Resistance;

        public enum ItemQuality
        {
            Common,
            Uncommon,
            Rare
        }
    }
}

