﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is simply a data container for our base pawns stats
/// Other monobehaviours on the pawn prefabs will take this data
/// and do things with it
/// </summary>

namespace AutoBattles
{
    [CreateAssetMenu(fileName = "New Pawn Stats", menuName = "Custom/PawnStats")]
    public class PawnStats : ScriptableObject
    {
        [Header("GENERAL INFO")]
        public new string name;
        public Sprite icon;
        public GameObject pawn;
        public StarRating starRating;
        public PawnQuality pawnQuality;
        [Tooltip("This is a reference to the PawnStats object that this PawnStats will upgrade into upon reaching 3 unique versions of itself. If not set, the pawn will not combine with others.")]
        public PawnStats upgradedPawn;

        [Header("ORIGINS")]
        public List<Origin> origins;

        [Header("CLASSES")]
        public List<Class> classes;

        [Header("ATK")]
        [Tooltip("Only set this if you want the pawn to spawn a projectile on attack (i.e. Ranged pawn)")]
        public GameObject projectilePrefab;

        [Header("AUTO ATTACK")]
        [Tooltip("Physical damage a pawn does on an auto attack.")]
        public int AAPhysicalDamage;
        [Tooltip("Magic damage a pawn does on an auto attack.")]
        public int AAMagicDamage;
        [Tooltip("True damage a pawn does on an auto attack.")]
        public int AATrueDamage;
        [Tooltip("The range of the pawns auto attacks (in squares, i.e. 1 = attack 1 tile away, 2 = attack 2 tile away")]
        public int AARange;
        [Tooltip("Pawn waits for 100/(100 + attack speed) seconds between attacks.")]
        public float attackSpeed;

        [Header("SPECIAL ATTACK")]
        [Tooltip("Physical damage a pawn does on a special attack.")]
        public int SAPhysicalDamage;
        [Tooltip("Magic damage a pawn does on a special attack.")]
        public int SAMagicDamage;
        [Tooltip("True damage a pawn does on a special attack.")]
        public int SATrueDamage;
        [Tooltip("Bonus attack speed granted by special attack.")]
        public int SAttackSpeed;
        [Tooltip("Healing granted by special attack.")]
        public int SAHeal;
        [Tooltip("How long buff from SA lasts.")]
        public int SADuration;
        [Tooltip("The range of the pawns special attack (in squares, i.e. 1 = attack 1 square away, 2 = attack 2 squares away")]
        public int SARange;
        [Tooltip("Bonus mana regenaration granted by special attack.")]
        public int SAManaRegen;
        [Tooltip("Bonus mana per attack granted by special attack.")]
        public int SAManaPerAttack;
        [Tooltip("Bonus armor granted by special attack.")]
        public int SAArmor;
        [Tooltip("Bonus resistance granted by special attack.")]
        public int SAResistance;

        [Header("OTHER STATS")]
        [Tooltip("The amount of mana a pawn will generate on each auto attack.")]
        public float manaPerAttack;
        [Tooltip("The amount of mana a pawn will generate on every second.")]
        public float manaRegen;
        [Tooltip("How fast the pawn can move around the chess board")]
        public float moveSpeed;

        [Header("DEF")]
        [Tooltip("The total amount of dmg a pawn can take before dying.")]
        public int health;
        [Tooltip("The total amount of mana a hero needs to accumulate before being able to cast their spell.")]
        public int mana;
        [Tooltip("Total armor used in calculating physical dmg reduction.")]
        public int armor;
        [Tooltip("Total resistance used in calculating magical dmg reduction.")]
        public int resistance;
        [Tooltip("Amount of health pawn regenerates every second.")]
        public float healing;

        public enum Origin
        {
            Orc,            
            Human            
        }

        public enum Class
        {
            Warrior,        
            Knight
        }

        public enum StarRating
        {
            One,
            Two,
            Three
        }

        public enum PawnQuality
        {
            Common,
            Uncommon,
            Rare
        }
    }
}

