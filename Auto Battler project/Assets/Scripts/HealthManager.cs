﻿using AutoBattles;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Auto_Battles_Engine.Assets.Scripts
{
    /// <summary>
    /// Takes care of player and enemy health bars (not pawn health bars)
    /// </summary>
    public class HealthManager : Singleton<HealthManager>
    {
        #region Variables

        [Header("Health")]
        [SerializeField]
        private int _currentPlayerHealth;
        [SerializeField]
        private int _currentEnemyHealth;
        [SerializeField]
        private Image _playerBar;
        [SerializeField]
        private Image _enemyBar;

        private int maxHealth;

        //references
        private UserInterfaceManager _userInterface;
        #endregion

        #region Properties
        public int CurrentPlayerHealth { get => _currentPlayerHealth; set => _currentPlayerHealth = value; }
        public int CurrentEnemyHealth { get => _currentEnemyHealth; set => _currentEnemyHealth = value; }
        public Image PlayerBar { get => _playerBar; set => _playerBar = value; }
        public Image EnemyBar { get => _enemyBar; set => _enemyBar = value; }

        //references
        protected UserInterfaceManager UserInterface { get => _userInterface; set => _userInterface = value; }
        #endregion

        #region Methods

        protected virtual void Awake()
        {
            UserInterface = UserInterfaceManager.Instance;
            if (!UserInterface)
            {
                Debug.LogError("No UserInterfaceManager singleton instance found in the scene. Please add an UserInterfaceManager script to the GameManager gamobject before entering playmode.");
            }
            if (!PlayerBar)
            {
                Debug.LogError("No PlayerBar reference found.");
            }
            if (!EnemyBar)
            {
                Debug.LogError("No EnemyBar reference found.");
            }
        }

        //decreses enemy health and scales health bar
        //checks if enemy is dead
        public virtual bool EnemyDecreaseHealth(int health)
        {
            CurrentEnemyHealth -= health;
            EnemyBar.fillAmount = (float) CurrentEnemyHealth / maxHealth;

            if (CurrentEnemyHealth <= 0)
            {
                return true;
            }
            return false;
        }

        //decreses player health and scales health bar
        //checks if player is dead
        public virtual bool PlayerDecreaseHealth(int health)
        {

            CurrentPlayerHealth -= health;
            PlayerBar.fillAmount = (float)CurrentPlayerHealth / maxHealth;

            if (CurrentPlayerHealth <= 0)
            {
                return true;
            }
            return false;
        }

        public virtual void ResetHealth(int startingHealth)
        {
            CurrentEnemyHealth = startingHealth;
            CurrentPlayerHealth = startingHealth;
            maxHealth = startingHealth;
            EnemyBar.fillAmount = 1;
            PlayerBar.fillAmount = 1;
        }
        #endregion
    }
}
