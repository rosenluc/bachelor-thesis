﻿using Assets.Auto_Battles_Engine.Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// This will be responsible for begining the round, ending the round, and
/// handling most of the games basic functions (rerolling the shop, leveling up,
/// etc).
/// </summary>

namespace AutoBattles
{
    public class GameManager : Singleton<GameManager>
    {
        #region Variables
        [Header("Global Variables")]
        [SerializeField]
        private bool _inCombat;
        [SerializeField]
        private int _currentRound;
        [SerializeField]
        [Tooltip("How much gold it will cost the player to reroll the pawn shop selection. Defaults to 2 if 0 at runtime.")]
        private int _rerollCost;
        [SerializeField]
        [Tooltip("How much gold the player will start the game with, will default to 2 if left at 0.")]
        private int _startingGold;
        [SerializeField]
        [Tooltip("How much HP player has at the beginning of the game.")]
        private int _startingHP;

        [Header("Round Specific Variables")]
        [SerializeField]
        private bool _roundIsEnding;
        [SerializeField]
        [Tooltip("This is the amount of time the game will wait at the first EndRound call before deciding a winner. Set to 1 by default if 0 at runtime.")]
        private float _endRoundLeewayDuration;
        [SerializeField]
        private bool _playerArmyWon;
        [SerializeField]
        private bool _enemyArmyWon;
        [SerializeField]
        private int _baseDamage;
        [SerializeField]
        private int _pawnDamage;

        private DateTime timer;
        private TimeSpan endRoundAfterSeconds;

        //references
        private UserInterfaceManager _UIManager;
        private PawnDatabase _pawnDatabaseScript;
        private ItemDatabase _itemDatabaseScript;
        private ArmyManager _armyManagerScript;
        private EnemyRosterManager _enemyRosterManagerScript;
        private PawnDragManager _pawnDragScript;
        private GoldManager _goldManagerScript;
        private ExperienceManager _expManager;
        private SynergyManager _synergyManagerScript;
        private HealthManager _healthManagerScript;
        private StatManager _statManagerScript;
        private Statistics _statisticsScript;

        private bool shopOpen;
        private bool menuOpen;
        private bool gameEnded = false;
        #endregion

        #region Properties
        //this is true while we are in the middle of a round of combat
        public bool InCombat { get => _inCombat; protected set => _inCombat = value; }

        //this will be the current round we are on and also correlate with which
        //enemy roster we will spawn at the start of a round, will default to 0 at start of runtime
        public int CurrentRound { get => _currentRound; set => _currentRound = value; }

        //how much gold it will cost the player to reroll the pawn shop selection
        protected int RerollCost { get => _rerollCost; set => _rerollCost = value; }

        //how much gold the player will start with
        public int StartingGold { get => _startingGold; set => _startingGold = value; }

        //if the round is currently in the process of ending
        protected bool RoundIsEnding { get => _roundIsEnding; set => _roundIsEnding = value; }

        //this is the amount of time the game will wait at the first EndRound call before deciding a winner
        protected float EndRoundLeewayDuration { get => _endRoundLeewayDuration; set => _endRoundLeewayDuration = value; }

        //this will be changed at the end of a round if the players army is victorious
        protected bool PlayerArmyWon { get => _playerArmyWon; set => _playerArmyWon = value; }

        //this will be changed at the end of a round if the enemies army is victorious
        protected bool EnemyArmyWon { get => _enemyArmyWon; set => _enemyArmyWon = value; }

        //this will be changed at the end of a round if the enemies army is victorious
        protected int StartingHP { get => _startingHP; set => _startingHP = value; }

        //references
        protected UserInterfaceManager UIManager { get => _UIManager; set => _UIManager = value; }
        protected PawnDatabase PawnDatabaseScript { get => _pawnDatabaseScript; set => _pawnDatabaseScript = value; }
        protected ItemDatabase ItemDatabaseScript { get => _itemDatabaseScript; set => _itemDatabaseScript = value; }
        protected ArmyManager ArmyManagerScript { get => _armyManagerScript; set => _armyManagerScript = value; }
        protected EnemyRosterManager EnemyRosterManagerScript { get => _enemyRosterManagerScript; set => _enemyRosterManagerScript = value; }
        protected PawnDragManager PawnDragScript { get => _pawnDragScript; set => _pawnDragScript = value; }
        protected GoldManager GoldManagerScript { get => _goldManagerScript; set => _goldManagerScript = value; }
        protected ExperienceManager ExpManager { get => _expManager; set => _expManager = value; }
        protected SynergyManager SynergyManagerScript { get => _synergyManagerScript; set => _synergyManagerScript = value; }
        protected HealthManager HealthManagerScript { get => _healthManagerScript; set => _healthManagerScript = value; }
        public int BaseDamage { get => _baseDamage; set => _baseDamage = value; }
        public int PawnDamage { get => _pawnDamage; set => _pawnDamage = value; }
        public StatManager StatManagerScript { get => _statManagerScript; set => _statManagerScript = value; }
        public Statistics StatisticsScript { get => _statisticsScript; set => _statisticsScript = value; }
        #endregion

        #region Methods
        protected virtual void Awake()
        {
            endRoundAfterSeconds = new TimeSpan(0, 0, 30);
            //Initialize references
            UIManager = UserInterfaceManager.Instance;
            if (!UIManager)
            {
                Debug.LogError("No UserInterfaceManager singleton instance found in the scene. Please add a UserInterfaceManager script to the Game Manager gameobject before" +
                    "entering playmode!");
            }

            SynergyManagerScript = SynergyManager.Instance;
            if (!SynergyManagerScript)
            {
                Debug.LogError("No SynergyManager singleton instance found in the scene. Please add a SynergyManager script to the Game Manager gameobject before" +
                    "entering playmode!");
            }

            PawnDatabaseScript = PawnDatabase.Instance;
            if (!PawnDatabaseScript)
            {
                Debug.LogError("No PawnDatabase singleton instance found in the scene. Please add a PawnDatabase script to the Database gameobject before" +
                      "entering playmode!");
            }

            ItemDatabaseScript = ItemDatabase.Instance;
            if (!ItemDatabaseScript)
            {
                Debug.LogError("No ItemDatabase singleton instance found in the scene. Please add a ItemDatabase script to the Database gameobject before" +
                      "entering playmode!");
            }

            ArmyManagerScript = ArmyManager.Instance;
            if (!ArmyManagerScript)
            {
                Debug.LogError("No ArmyManager singleton instance found in the scene. Please add a ArmyManager script to the Game Manager gameobject before" +
                      "entering playmode!");
            }

            EnemyRosterManagerScript = EnemyRosterManager.Instance;
            if (!EnemyRosterManagerScript)
            {
                Debug.LogError("No EnemyRosterManager singleton instance found in the scene. Please add a EnemyRosterManager script to the Game Manager gameobject before" +
                      "entering playmode!");
            }

            PawnDragScript = PawnDragManager.Instance;
            if (!PawnDragScript)
            {
                Debug.LogError("No PawnDragManager singleton instance found in the scene. Pleas add a PawnDragManager script to the Game Manager gameobject before" +
                    "entering playmode.");
            }

            GoldManagerScript = GoldManager.Instance;
            if (!GoldManagerScript)
            {
                Debug.LogError("No GoldManager singleton instance found! Please add a GoldManager script to the scene.");
            }

            ExpManager = ExperienceManager.Instance;
            if (!ExpManager)
            {
                Debug.LogError("No ExperienceManager singleton instance found! Please add a ExperienceManager script to the scene.");
            }

            HealthManagerScript = HealthManager.Instance;
            if (!HealthManagerScript)
            {
                Debug.LogError("No HealthManager singleton instance found! Please add a HealthManager script to the scene.");
            }

            StatisticsScript = Statistics.Instance;
            if (!StatisticsScript)
            {
                Debug.LogError("No Statistics singleton instance found! Please add a Statistics script to the scene.");
            }

            //if we didnt set a leeyway duration in the inspector, set it to 1
            if (EndRoundLeewayDuration == 0)
            {
                EndRoundLeewayDuration = 1f;
            }

            //set to 2 by default if 0 at runtime
            if (RerollCost == 0)
            {
                RerollCost = 2;
            }

            //make sure we dont accidently start the player with no money
            if (StartingGold == 0)
            {
                StartingGold = 2;
            }

            if (StartingHP == 0)
            {
                StartingHP = 100;
            }

            //Setup Scene
            CurrentRound = 0;

            //UI setup
            UIManager.CreateShopSlots();
        }

        protected virtual void Start()
        {
            //begin game
            BeginGame();
        }

        //increases the round number and changes damage values if needed
        protected virtual void IncreaseRoundCounter()
        {
            CurrentRound++;

            switch (CurrentRound)
            {
                case 1:
                    BaseDamage = 1;
                    PawnDamage = 1;
                    break;
                case 3:
                    BaseDamage = 2;
                    PawnDamage = 2;
                    break;
                case 6:
                    BaseDamage = 4;
                    PawnDamage = 3;
                    break;
                case 9:
                    BaseDamage = 6;
                    PawnDamage = 4;
                    break;
                case 12:
                    BaseDamage = 8;
                    PawnDamage = 5;
                    break;
                case 15:
                    BaseDamage = 10;
                    PawnDamage = 6;
                    break;
                case 18:
                    BaseDamage = 12;
                    PawnDamage = 7;
                    break;
                case 21:
                    BaseDamage = 16;
                    PawnDamage = 9;
                    break;
                case 24:
                    BaseDamage = 20;
                    PawnDamage = 11;
                    break;
                case 27:
                    BaseDamage = 24;
                    PawnDamage = 14;
                    break;
            }

            UIManager.UpdateCurrentRoundText(CurrentRound);
        }

        //this is the special function we call once at the start of the game to start us on our journey
        //this is very similiar to an 'end of round' function but we keep them seperate
        public virtual void BeginGame()
        {
            //Setup Scene
            CurrentRound = 0;
            BaseDamage = 1;
            PawnDamage = 0;
            InCombat = false;
            RoundIsEnding = false;

            GoldManagerScript.ResetGold();

            ExpManager.ResetExperience();
            EnemyRosterManagerScript.ResetRoster();
            ArmyManagerScript.ResetArmy();
            HealthManagerScript.ResetHealth(StartingHP);
            StatisticsScript.ResetStats();
            StatManagerScript.HidePanel();
            StatManagerScript.Initialized = false;
            UIManager.ResetItems();

            //name is a bit misleading, but we start the game with a free roll as if it were
            //the end of a round
            RerollShopAtEndofRound();

            //again, sort of misleading, but we need to increase the round by 1 at the start so we 'start' at round 1 instead of 0
            IncreaseRoundCounter();

            //Grand the player gold to start the game
            GoldManagerScript.GainGold(StartingGold);
            StatisticsScript.GoldEarnedAll += StartingGold;

            //give 1 exp to level the player to level 1
            GrantEndOfRoundExp();

            if (menuOpen)
            {
                UIManager.MenuPanel.Close();
                menuOpen = false;
            }

        }

        //this will get called at the start of every round
        public virtual void BeginRound()
        {
            //avoid calling this more than once
            if (InCombat)
                return;

            timer = DateTime.Now;

            //add gold to stat tracking at the start of the round
            StatisticsScript.GoldByRounds.Add(GoldManagerScript.CurrentGold);

            //close the army count display
            UIManager.ArmyCountDisplay.Close();

            //close the shop in case its open
            if (shopOpen)
            {
                UIManager.CloseShopMenu();
                shopOpen = false;
            }

            //reset clicked on pawn to avoid issues
            if (PawnDragScript.IsClickedOnPawn())
            {
                PawnDragScript.ClearClickedPawn();
            }

            //if we are currently dragging a pawn, send it back to where we got it
            if (PawnDragScript.IsDraggingPawn())
            {
                PawnDragScript.SendPawnBack();
            }

            //have enemy roster change before the combat starts
            EnemyRosterManagerScript.DecideMove();

            //SOLIDIFY PLAYER ROSTER

            //first make sure we arent currently over our max allowed active pawns
            ArmyManagerScript.CheckIfPlayerIsOverMaxArmySize();

            //set the player roster for reseting after combat
            ArmyManagerScript.SetPlayerRoster();

            //CHECK FOR SYNERGY BUFFS ON EITHER TEAM

            //first check with the players army being the 'friendly' army
            SynergyManagerScript.ApplySynergyEffects(ArmyManagerScript.ActivePlayerPawns, ArmyManagerScript.ActiveEnemyPawns);

            //then check with the enemies army being the 'friendly' army
            SynergyManagerScript.ApplySynergyEffects(ArmyManagerScript.ActiveEnemyPawns, ArmyManagerScript.ActivePlayerPawns);

            //tell our HealthAndMana script to update the current health of pawns in case they gained/loss health from synergies
            foreach (GameObject pawn in ArmyManagerScript.ActivePlayerPawns)
            {
                pawn.GetComponent<HealthAndMana>().StartOfCombatHealthRefresh();
            }

            foreach (GameObject pawn in ArmyManagerScript.ActiveEnemyPawns)
            {
                pawn.GetComponent<HealthAndMana>().StartOfCombatHealthRefresh();
            }

            StatManagerScript.ResetValues();

            //Let all active pawns on enemy and player side know they are in combat            
            foreach (GameObject pawn in ArmyManagerScript.ActivePlayerPawns)
            {
                pawn.GetComponent<Status>().BeginCombat();
            }

            foreach (GameObject pawn in ArmyManagerScript.ActiveEnemyPawns)
            {
                pawn.GetComponent<Status>().BeginCombat();
            }

            InCombat = true;

            //start coroutines that manage passive healing and mana regeneration
            InvokeRepeating("ManaRegeneration", 0, 1);
            InvokeRepeating("Healing", 0, 1);

            //check if either army is at 0 at the start of the round
            ArmyManagerScript.CheckIfEnemyWonRound();
            ArmyManagerScript.CheckIfPlayerWonRound();
        }

        //adds mana to pawns each second
        private void ManaRegeneration()
        {
            foreach (GameObject pawn in ArmyManagerScript.ActivePlayerPawns)
            {
                pawn.GetComponent<HealthAndMana>().GainMana(pawn.GetComponent<Pawn>().ManaRegen);
            }

            foreach (GameObject pawn in ArmyManagerScript.ActiveEnemyPawns)
            {
                pawn.GetComponent<HealthAndMana>().GainMana(pawn.GetComponent<Pawn>().ManaRegen);
            }
        }

        //adds health to pawns each second
        private void Healing()
        {
            foreach (GameObject pawn in ArmyManagerScript.ActivePlayerPawns)
            {
                float heal = pawn.GetComponent<Pawn>().Healing * (1 + pawn.GetComponent<Pawn>().ItemHealing);
                pawn.GetComponent<HealthAndMana>().GainHealth(heal);

                //write heal into stats
                pawn.GetComponent<IngameStats>().AddHealthHealed(heal, 0);
                if (pawn.GetComponent<Pawn>().HasItem)
                    pawn.GetComponent<Pawn>().EquippedItem.StatHealth += heal - pawn.GetComponent<Pawn>().Healing;

            }

            foreach (GameObject pawn in ArmyManagerScript.ActiveEnemyPawns)
            {
                float heal = pawn.GetComponent<Pawn>().Healing * (1 + pawn.GetComponent<Pawn>().ItemHealing);
                pawn.GetComponent<HealthAndMana>().GainHealth(heal);
                if (pawn.GetComponent<Pawn>().HasItem)
                    pawn.GetComponent<Pawn>().EquippedItem.StatHealth += heal - pawn.GetComponent<Pawn>().Healing;

            }
        }

        #region End of round methods

        public virtual void PlayerWonRound()
        {
            PlayerArmyWon = true;

            if (!RoundIsEnding)
            {
                RoundIsEnding = true;

                EndRound();
            }
        }

        public virtual void EnemyWonRound()
        {
            EnemyArmyWon = true;


            if (!RoundIsEnding)
            {
                RoundIsEnding = true;

                EndRound();
            }
        }

        //this calls everything resposible for bringing the current round to an end
        protected virtual void EndRound()
        {
            //stop passive healing and mana regeneration
            CancelInvoke();
            //stop undating statistics
            StatManagerScript.StopTick();

            //gain gold from survivng human units if synergy is active
            int g = ArmyManagerScript.CalculatePlayerGoldFromSynergy();
            //write gold into stat tracking
            GoldManagerScript.GainGold(g);
            StatisticsScript.ManageGoldEarned(0, 0, g);

            //give enemy gold
            EnemyRosterManagerScript.GainGold(ArmyManagerScript.CalculateEnemyGoldFromSynergy());

            //let all pawns from that round know that combat has finished
            //this will also reset synergy bonuses for player pawns
            ArmyManagerScript.EndCombatForAllActivePawns();

            //start the timer to decide a winner
            StartCoroutine(CountdownToDecideWinner());

            //start timer for rewards and board reset
            StartCoroutine(ResetBoardAndGiveRewards());
        }

        //this will give a little leeway at the end of the round in case both armies killed
        //each other and it is a draw
        protected virtual IEnumerator CountdownToDecideWinner()
        {
            yield return new WaitForSeconds(EndRoundLeewayDuration);

            DecideWinnerOfRound();
        }

        //decides the winner of the round and then resets the chess board
        protected virtual void DecideWinnerOfRound()
        {
            if (PlayerArmyWon && EnemyArmyWon)
            {
                //both armies won, draw
                UIManager.UpdateWinnerMessageText("It's a Draw!");
            }
            else if (PlayerArmyWon)
            {
                //player army won
                UIManager.UpdateWinnerMessageText("You Won the Round!");

                //add one extra gold to the player
                GoldManagerScript.GainGold(1);
                StatisticsScript.ManageGoldEarned(1, 0, 0);
            }
            else if (EnemyArmyWon)
            {
                //enemy army won
                UIManager.UpdateWinnerMessageText("Enemy Won the Round!");

                //add one extra coin to the enemy
                EnemyRosterManagerScript.GainGold(1);
            }

            //tell the UI to display the winner message
            UIManager.WinnerMessagePanel.Open();
        }

        //called once the round has offically ended
        //responsible for reseting the players board back to before combat status
        //and giving the player gold/exp
        protected virtual IEnumerator ResetBoardAndGiveRewards()
        {
            yield return new WaitForSeconds(3);

            //calculate pawn and base damage
            int pawnDmgP = ArmyManagerScript.ActivePlayerPawns.Count * PawnDamage;
            int pawnDmgE = ArmyManagerScript.ActiveEnemyPawns.Count * PawnDamage;
            int playerDamage = pawnDmgP + BaseDamage;
            int enemyDamage = pawnDmgE + BaseDamage;

            ResetChessBoard();

            //close the winner message
            UIManager.WinnerMessagePanel.Close();

            if(EnemyArmyWon && PlayerArmyWon)
            {
                //add damage to stat tracking
                StatisticsScript.ManageDamageDealt(pawnDmgP, BaseDamage, pawnDmgE, BaseDamage);

                //tie, deal damage to both
                //if enemy or player dies, game ends
                if (HealthManagerScript.EnemyDecreaseHealth(playerDamage))
                {
                    EndGame(true);
                }
                else if (HealthManagerScript.PlayerDecreaseHealth(enemyDamage))
                {
                    EndGame(false);
                }
            }
            else if (EnemyArmyWon)
            {
                //add damage to stat tracking
                StatisticsScript.ManageDamageDealt(0, 0, pawnDmgE, BaseDamage);

                //enemy won, deal damage to player
                //if player dies, game ends
                if (HealthManagerScript.PlayerDecreaseHealth(playerDamage))
                {
                    EndGame(false);
                }
            }
            else if (PlayerArmyWon)
            {
                //add damage to stat tracking
                StatisticsScript.ManageDamageDealt(pawnDmgP, BaseDamage, 0, 0);

                //player won, deal damage to enemy
                //if enemy dies, game ends
                if (HealthManagerScript.EnemyDecreaseHealth(enemyDamage))
                {
                    EndGame(true);
                }
            }
            else
            {
                //add damage to stat tracking
                StatisticsScript.ManageDamageDealt(pawnDmgP, BaseDamage, pawnDmgE, BaseDamage);

                //tie, deal damage to both
                //if enemy or player dies, game ends
                if (HealthManagerScript.EnemyDecreaseHealth(playerDamage))
                {
                    EndGame(true);
                }
                else if (HealthManagerScript.PlayerDecreaseHealth(enemyDamage))
                {
                    EndGame(false);
                }
            }

            //reset these
            EnemyArmyWon = false;
            PlayerArmyWon = false;

            //increase current round
            IncreaseRoundCounter();

            GrantEndOfRoundExp();

            GrantEndOfRoundGold();

            //reroll the shop for free
            RerollShopAtEndofRound();

            OfferItems();
        }

        protected void EndGame(bool playerWon)
        {
            gameEnded = true;

            //save the finished match to json
            SaveMatch(playerWon);

            //update message in the menu
            if (playerWon)
            {
                UIManager.UpdateEndMessageText("You won the game!");
            }
            else
            {
                UIManager.UpdateEndMessageText("You lost the game!");
            }

            //open menu
            if (!menuOpen)
            {
                UIManager.MenuPanel.Open();
                menuOpen = true;
            }
        }

        //converts star count of a pawn to an integer
        private int CountingStars(Pawn p)
        {
            switch (p.Stats.starRating)
            {
                case PawnStats.StarRating.One:
                    return 1;
                case PawnStats.StarRating.Two:
                    return 2;
                case PawnStats.StarRating.Three:
                    return 3;
            }
            return 0;
        }

        //saves match into json
        private void SaveMatch(bool win)
        {
            List<Pawn> p = ArmyManagerScript.TotalRosterAsPawns();
            List<string> pawns = new List<string>();
            List<string> items = new List<string>();
            List<int> stars = new List<int>();

            //save all active pawns, their stars and items, into lists
            foreach (Pawn pawn in p)
            {
                pawns.Add(pawn.Stats.name);
                stars.Add(CountingStars(pawn));
                if (pawn.HasItem)
                {
                    items.Add(pawn.EquippedItem.Name);
                }
                else
                {
                    items.Add(null);
                }
            }

            //count active traits in synergies
            int hs = 0;
            int os = 0;
            int ws = 0;
            int ks = 0;
            foreach(SynergyObject synergy in SynergyManagerScript.synergies)
            {

                if(synergy.widgetScript.Synergy.name == "Human")
                {
                    hs = synergy.uniqueActivePawns;
                }
                else if (synergy.widgetScript.Synergy.name == "Orc")
                {
                    os = synergy.uniqueActivePawns;
                }
                else if (synergy.widgetScript.Synergy.name == "Warrior")
                {
                    ws = synergy.uniqueActivePawns;
                }
                else
                {
                    ks = synergy.uniqueActivePawns;
                }
            }
            
            //create a Roster with the values we got
            Roster save = new Roster(hs, os, ws, ks, StatisticsScript.GoldByRounds, pawns, items, stars, win);

            int count = 0;
            string filePath = Application.persistentDataPath + "/matches";
            //count how many saves we have so far
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            else
            {
                DirectoryInfo info = new DirectoryInfo(filePath);
                FileInfo[] fileInfo = info.GetFiles("*.json");
                foreach (FileInfo f in fileInfo)
                {
                    count++;
                }
            }

            //convert the Roster object to string
            string json = JsonUtility.ToJson(save);

            //generate unique file name
            string name = filePath + "/match" + count + ".json";
            //write string into json
            File.WriteAllText(name, json);
        }

        //we end combat in this function because otherwise we would be able to move
        //pawns around on the board pre-maturely and mess things up
        protected virtual void ResetChessBoard()
        {
            //reset enemy and player rosters
            ArmyManagerScript.ResetEnemyRoster();
            ArmyManagerScript.ResetActivePlayerPawns();

            //open the army count display again
            UIManager.ArmyCountDisplay.Open();

            //change this back now that the round has totally finished
            RoundIsEnding = false;
            InCombat = false;
        }

        protected virtual void GrantEndOfRoundGold()
        {
            //calculate interest
            double goldGain = Math.Floor(GoldManagerScript.CurrentGold/10.0);
            
            if(goldGain > 5)
            {
                goldGain = 5;
            }

            //write gold into stat tracking
            StatisticsScript.ManageGoldEarned(0, (int)goldGain, 0);
            //add base income
            goldGain += 3;

            GoldManagerScript.GainGold((int)goldGain);
            StatisticsScript.GoldEarnedAll += 3;

            //do the same for enemy
            goldGain = Math.Floor(EnemyRosterManagerScript.Gold / 10.0);

            if (goldGain > 5)
            {
                goldGain = 5;
            }
            goldGain += 3;

            EnemyRosterManagerScript.GainGold((int)goldGain);
        }

        protected virtual void GrantEndOfRoundExp()
        {
            ExpManager.GainExperience(1);
            EnemyRosterManagerScript.GainExperience(1);
        }

        #endregion

        #region shop methods
        //This will be called when the player presses the reroll shop button, it will create an array of random pawns and then 
        //send that array to the UserInterfaceManager script to be displayed to the player if the player has enough gold
        public virtual void RerollShopSlots()
        {
            //check if we have the gold to reroll
            //if we do, reroll it
            if (GoldManagerScript.SpendGold(RerollCost))
            {
                StatisticsScript.ManageGoldSpent(RerollCost, 0, 0);
                PawnStats[] newPawns = RerollShop();

                //Once the for loop is completed and our newPawns array is filled
                //pass the array to the UserInterfaceManager to be displayed to the player
                UIManager.DisplayNewShopLineUp(newPawns);

                //Add this here in case we ever add a reroll hotkey usable when shop is closed
                //won't matter if the shop is already open
                if (!shopOpen)
                {
                    UIManager.OpenShopMenu();
                    shopOpen = true;
                }
            }
            //otherwise, let the player know he needs more gold
            else
            {
                print("Need more gold to reroll.");
            }
        }

        //generates list of pawns according to current rolling chances
        public PawnStats[] RerollShop()
        {
            PawnStats[] newPawns = new PawnStats[UIManager.ShopSlotCount];

            int commonChance = 100;
            int uncommonChance = 100;
            int rareChance = 100;

            //set chances according to player's level
            switch (ExpManager.CurrentLevel)
            {
                case 0:
                    commonChance = 100;
                    uncommonChance = 0;
                    rareChance = 0;
                    UIManager.ChangeRerollChances(commonChance, uncommonChance, rareChance);
                    break;
                case 1:
                    commonChance = 100;
                    uncommonChance = 0;
                    rareChance = 0;
                    break;
                case 2:
                    commonChance = 80;
                    uncommonChance = 20;
                    rareChance = 0;
                    break;
                case 3:
                    commonChance = 60;
                    uncommonChance = 39;
                    rareChance = 1;
                    break;
                case 4:
                    commonChance = 40;
                    uncommonChance = 52;
                    rareChance = 8;
                    break;
                case 5:
                    commonChance = 20;
                    uncommonChance = 63;
                    rareChance = 17;
                    break;
                case 6:
                    commonChance = 5;
                    uncommonChance = 65;
                    rareChance = 30;
                    break;
            }

            //for loop that will iterate the number of times based on 
            //how many shop slots we specified in the user interface manager
            for (int i = 0; i < UIManager.ShopSlotCount; i++)
            {
                //this will give us a random index based on the size of our pawn list in the 
                //pawn database (all possible pawns that can be used in playmode)
                int randomInt = UnityEngine.Random.Range(0, 100);

                if (randomInt < commonChance)
                {
                    randomInt = UnityEngine.Random.Range(0, 3);
                }
                else if (randomInt < commonChance + uncommonChance)
                {
                    randomInt = UnityEngine.Random.Range(3, 6);
                }
                else
                {
                    randomInt = UnityEngine.Random.Range(6, 8);
                }

                //set our current iteration in newPawns to our corresponding pawn generated
                //by our randomIndex
                newPawns[i] = PawnDatabaseScript.Pawns[randomInt];
            }
            return newPawns;
        }

        //this is called at the end of each round after rewards are handed out
        //rerolls the shop for free for the player
        protected virtual void RerollShopAtEndofRound()
        {
            //create an array of pawns with the size of shop slots we set
            //in the user interface manager
            PawnStats[] newPawns = RerollShop();

            //Once the for loop is completed and our newPawns array is filled
            //pass the array to the UserInterfaceManager to be displayed to the player
            UIManager.DisplayNewShopLineUp(newPawns);

            //bring the shop menu up to display to the player
            if (!shopOpen)
            {
                UIManager.OpenShopMenu();
                shopOpen = true;
            }    
        }

        //called when we press the Level button in the shop
        public virtual void PurchaseExperience()
        {
            if (GoldManagerScript.SpendGold(5))
            {
                StatisticsScript.ManageGoldSpent(0, 5, 0);
                ExpManager.GainExperience(4);
            }
            else
            {
                print("Not enough gold!");
            }
        }

        //this is only called when the player starts the round with more active pawns
        //than allowed and has a full bench, will sell the overage of active pawns
        public virtual void SellActivePawn(GameObject pawn)
        {
            UIManager.RemoveItemFromPawn(pawn.GetComponent<Pawn>());

            //remove from active player roster
            ArmyManagerScript.RemoveActivePawnFromPlayerRoster(pawn);

            //remove from total player roster
            ArmyManagerScript.RemovePawnFromTotalPlayerRoster(pawn);

            //grab reference
            Status status = pawn.GetComponent<Status>();

            //give the player the gold worth equivalent of the pawn we are selling
            GoldManagerScript.GainGold(status.GoldWorth);
            StatisticsScript.PawnSold(status.GoldWorth);

            //actually destroy the pawn
            status.SelfDestruct();
        }

        #endregion

        //called when the player ends game from the menu
        public void EndGame()
        {
            Application.Quit();
        }

        //offers three items to the player each 5 rounds
        private void OfferItems()
        {
            //if the round isn't correct, return
            if (CurrentRound%5 != 0 || CurrentRound == 0)
            {
                return;
            }

            //set item chances according to round number
            int commonChance = 100;
            int uncommonChance = 100;
            int rareChance = 100;

            switch (CurrentRound)
            {
                case 5:
                    uncommonChance = 0;
                    rareChance = 0;
                    break;
                case 10:
                    commonChance = 50;
                    uncommonChance = 50;
                    rareChance = 0;
                    break;
                case 15:
                    commonChance = 20;
                    uncommonChance = 60;
                    rareChance = 20;
                    break;
                case 20:
                    commonChance = 10;
                    uncommonChance = 40;
                    rareChance = 50;
                    break;
                default:
                    commonChance = 10;
                    uncommonChance = 40;
                    rareChance = 50;
                    break;
            }

            int item1 = -1;
            int item2 = -1;
            int item3 = -1;
            int loops = 3;

            //loop through items until we generate three different random items
            for(int i = 0; i < loops; i++)
            {
                int randomIndex = UnityEngine.Random.Range(0, 100);

                if (randomIndex < commonChance)
                {
                    randomIndex = UnityEngine.Random.Range(0, 4);
                }
                else if (randomIndex < commonChance + uncommonChance)
                {
                    randomIndex = UnityEngine.Random.Range(4, 7);
                }
                else
                {
                    randomIndex = UnityEngine.Random.Range(7, 11);
                }

                if (item1 == -1)
                {
                    item1 = randomIndex;
                }else if(item2 == -1)
                {
                    if(randomIndex == item1)
                    {
                        loops++;
                        continue;
                    }
                    item2 = randomIndex;
                }
                else
                {
                    if (randomIndex == item1 || randomIndex == item2)
                    {
                        loops++;
                        continue;
                    }
                    item3 = randomIndex;
                }
            }

            //show the panel with the generated items
            UIManager.ShowItemChoice(ItemDatabaseScript.Items[item1], ItemDatabaseScript.Items[item2], ItemDatabaseScript.Items[item3]);

            //generate items for enemy
            ItemStats item = null;
            while (item == null || item.name == "Orc Emblem" || item.name == "Human Emblem")
            {
                int randomint = UnityEngine.Random.Range(0, 3);
                switch (randomint)
                {
                    case 0:
                        item = ItemDatabaseScript.Items[item1];
                        break;
                    case 1:
                        item = ItemDatabaseScript.Items[item2];
                        break;
                    case 2:
                        item = ItemDatabaseScript.Items[item3];
                        break;
                }
            }

            EnemyRosterManagerScript.PlaceItem(item);
        }

        #region Stat methods
        //these methods are called when button on the stat panel is clicked

        public void SwitchToDamageDealt()
        {
            StatManagerScript.SwitchToDamageDealt();
        }

        public void SwitchToDamageTaken()
        {
            StatManagerScript.SwitchToDamageTaken();
        }

        public void SwitchToDamageBlocked()
        {
            StatManagerScript.SwitchToDamageBlocked();
        }

        public void SwitchToHealthHealed()
        {
            StatManagerScript.SwitchToHealthHealed();
        }
        #endregion

        //update method contains some shortcuts
        protected void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!menuOpen && !UIManager.EconomyStatsPanel.isActive && !InCombat && !RoundIsEnding)
                {
                    UIManager.UpdateEndMessageText("Options");
                    UIManager.MenuPanel.Open();
                    menuOpen = true;
                }
                else if(!gameEnded)
                {
                    UIManager.MenuPanel.Close();
                    UIManager.EconomyStatsPanel.Close();
                    menuOpen = false;
                }
            }else if (Input.GetKeyDown("s"))
            {
                if (!shopOpen)
                {
                    UIManager.OpenShopMenu();
                }
                else
                {
                    UIManager.CloseShopMenu();
                }
                shopOpen = !shopOpen;
            }else if (Input.GetKeyDown("x"))
            {
                if(!InCombat && !RoundIsEnding)
                {
                    BeginRound();
                }
            }
            if(DateTime.Now - timer > endRoundAfterSeconds && InCombat && !RoundIsEnding)
            {
                EnemyArmyWon = true;
                PlayerArmyWon = true;


                if (!RoundIsEnding)
                {
                    RoundIsEnding = true;

                    EndRound();
                    ArmyManagerScript.KillEveryone();
                }
            }
        }

        #endregion
    }
}

