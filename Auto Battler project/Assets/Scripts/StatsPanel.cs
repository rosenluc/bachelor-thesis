﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/// <summary>
/// This script is attached to the Stats Panel in the UI Canvas and is
/// responsible for updating that panel with the currently selected pawns stats
/// </summary>

namespace AutoBattles
{
    public class StatsPanel : Singleton<StatsPanel>
    {
        #region Variables
        [SerializeField]
        private GameObject _pawn;
        private Pawn _pawnScript;
        private PawnStats _pawnStats;
        private HealthAndMana _healthAndManaScript;

        [Header("Stat UI References")]
        [SerializeField]
        protected Text pawnName;
        [SerializeField]
        protected GameObject oneStarParent;
        [SerializeField]
        protected GameObject twoStarParent;
        [SerializeField]
        protected GameObject threeStarParent;
        [SerializeField]
        protected Image pawnIcon;
        [SerializeField]
        protected Text pawnHealth;
        [SerializeField]
        protected Text pawnDefense;
        [SerializeField]
        protected Text pawnAADamage;
        [SerializeField]
        protected Text pawnAttackSpeed;
        [SerializeField]
        protected Text pawnManaRegen;
        [SerializeField]
        protected Text pawnSADescription;
        [SerializeField]
        private Image itemIcon;
        [SerializeField]
        private Image itemFill1;
        [SerializeField]
        private Image itemFill2;
        [SerializeField]
        private Text itemText;
        [SerializeField]
        private GameObject itemPanel;

        //references
        private PawnDragManager _pawnDragScript;
       
        #endregion

        #region Properties
        //this property will hold the pawn who's stats we want to display
        protected GameObject Pawn { get => _pawn; set => _pawn = value; }

        protected PawnStats PawnStats { get => _pawnStats; set => _pawnStats = value; }
        protected HealthAndMana HealthAndManaScript { get => _healthAndManaScript; set => _healthAndManaScript = value; }
        protected Pawn PawnScript { get => _pawnScript; set => _pawnScript = value; }

        //references
        protected PawnDragManager PawnDragScript { get => _pawnDragScript; set => _pawnDragScript = value; }
        #endregion

        #region Methods
        protected virtual void Awake()
        {
            //initialize references
            PawnDragScript = PawnDragManager.Instance;
            if (!PawnDragScript)
            {
                Debug.LogError("No 'PawnDragManager' singleton instance found in the scene. Please add one before entering playmode.");
            }
            itemPanel.SetActive(false);
        }        

        //this is a healtier alternative to just putting our code in 'Update'
        IEnumerator StatTick()
        {
            yield return new WaitForSeconds(0.1f);

            RefreshStats();

            StartCoroutine("StatTick");
        }

        //called when we want to display the stats of a new pawn
        public virtual void DisplayNewPawnStats(GameObject pawn)
        {
            //set reference to the new pawn who's stats we
            //want to display
            Pawn = pawn;

            //set reference to the actual pawnstats component
            PawnScript = pawn.GetComponent<Pawn>();
            PawnStats = PawnScript.Stats;
            HealthAndManaScript = Pawn.GetComponent<HealthAndMana>();

            RefreshStats();

            //make sure we start our coroutine
            StartCoroutine("StatTick");
        }

        //called to stop our corountine from updating the stats
        public virtual void StopStatTick()
        {
            StopCoroutine("StatTick");
        }

        //called everytime 'RefreshStats' ticks
        protected virtual void RefreshStats()
        {
            if (Pawn != null)
            {
                //either set item stats to refresh or hide them
                if (PawnScript.HasItem)
                {
                    RefreshItemStats(PawnScript.EquippedItem);
                    itemPanel.SetActive(true);
                }
                else
                {
                    itemPanel.SetActive(false);
                }

                //refresh the stats

                //name
                pawnName.text = PawnStats.name;

                //Star Quality
                if (PawnStats.starRating == PawnStats.StarRating.One)
                {
                    oneStarParent.SetActive(true);
                    twoStarParent.SetActive(false);
                    threeStarParent.SetActive(false);
                }
                else if (PawnStats.starRating == PawnStats.StarRating.Two)
                {
                    oneStarParent.SetActive(false);
                    twoStarParent.SetActive(true);
                    threeStarParent.SetActive(false);
                }
                else if (PawnStats.starRating == PawnStats.StarRating.Three)
                {
                    oneStarParent.SetActive(false);
                    twoStarParent.SetActive(false);
                    threeStarParent.SetActive(true);
                }

                //icon
                pawnIcon.sprite = PawnStats.icon;

                //health
                string healthText = HealthAndManaScript.CurrentHealth.ToString("F0") + " / " + (PawnScript.Health + PawnScript.SynergyHealth);
                float healing = (PawnScript.Healing + PawnScript.SynergyHealing) * (1 + PawnScript.ItemHealing * PawnScript.SynergyBuffs);
                if (healing > 0)
                {
                    healthText += "\n + " + healing + " / s";
                }
                pawnHealth.text = healthText;

                //defense
                float armor = (PawnScript.Armor + PawnScript.BonusArmor * PawnScript.ItemBuff + PawnScript.SynergyArmor) * (1 + PawnScript.ItemArmor * PawnScript.SynergyBuffs);
                float resistance = (PawnScript.Resistance + PawnScript.BonusResistance * PawnScript.ItemBuff + PawnScript.SynergyResistance) * (1 + PawnScript.ItemResistance * PawnScript.SynergyBuffs);

                pawnDefense.text = armor + "A, " + resistance + "R";

                //AAdamage
                float phys = PawnScript.AAPhysicalDamage + PawnScript.SynergyPhysicalDamage;
                float mag = PawnScript.AAMagicDamage + PawnScript.SynergyMagicDamage;
                float tru = PawnScript.AATrueDamage + PawnScript.SynergyTrueDamage;

                pawnAADamage.text = phys + " + " + mag + " + " + tru;

                //attack speed
                pawnAttackSpeed.text = PawnScript.AttackSpeed.ToString();

                //mana regen
                float reg = PawnScript.ManaRegen;
                float pa = PawnScript.ManaPerAttack;
                pawnManaRegen.text = reg + "/s + " + pa + "/att";

                //move speed
                pawnSADescription.text = PawnScript.CalculateDescription();
            }
            else
            {
                PawnDragScript.CloseStatsWindow();
            }
        }

        //if the displayed pawn has item, its stat updates periodically
        private void RefreshItemStats(Item item)
        {
            //icon
            itemIcon.sprite = item.Stats.icon;
            //get the stats affected by this item
            Tuple<float, float> tuple = item.ChangingStats();
            float f1 = tuple.Item1;
            float f2 = tuple.Item2;
            float maxWidth = itemFill1.transform.parent.gameObject.GetComponent<Image>().rectTransform.sizeDelta.x - 1.5f;
            Vector2 width = itemFill1.rectTransform.sizeDelta;
            //set the item bar so that it fits
            while(f1 * item.StatRatio > maxWidth)
            {
                item.StatRatio *= 0.75f;
            }
            while ((f2 + f1) * item.StatRatio > maxWidth)
            {
                item.StatRatio *= 0.75f;
            }
            itemFill1.rectTransform.sizeDelta = new Vector2(f1 * item.StatRatio, width.y);
            itemFill2.rectTransform.sizeDelta = new Vector2((f1 + f2) * item.StatRatio, width.y);

            //the value in bar as a number
            itemText.text = (f1 + f2).ToString();
        }

        #endregion
    }
}
